//
//  NSString+ARYellowPage.h
//  ARYellowPage
//
//  Created by youdian on 2018/4/17.
//  Copyright © 2018年 YouDianAd. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (ARYellowPage)

/**
 判断是否是手机号
 * @ param mobileNumber 手机号
 *
 @ return YES是 NO不是
 */
+ (BOOL) isMobile:(NSString *)mobileNumber;
@end
