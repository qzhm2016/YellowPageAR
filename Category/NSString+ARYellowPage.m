//
//  NSString+ARYellowPage.m
//  ARYellowPage
//
//  Created by youdian on 2018/4/17.
//  Copyright © 2018年 YouDianAd. All rights reserved.
//

#import "NSString+ARYellowPage.h"

@implementation NSString (ARYellowPage)
+ (BOOL) isMobile:(NSString *)mobileNumber{
    NSString *phoneRegex = @"^((13[0-9])|(15[^4,\\D])|(18[0-9])|(14[57])|(17[013678]))\\d{8}$";
    NSPredicate *phoneTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@",phoneRegex];
    return [phoneTest evaluateWithObject:mobileNumber];
}
@end
