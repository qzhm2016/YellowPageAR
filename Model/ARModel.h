//
//  ARModel.h
//  ARYellowPage
//
//  Created by youdian on 2018/4/19.
//  Copyright © 2018年 YouDianAd. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ARModel : NSObject

@end

@interface Banner : ARModel

@property (nonatomic ,copy) NSString *cityid;
@property (nonatomic ,copy) NSString *des;
@property (nonatomic ,copy) NSString *id;
@property (nonatomic ,copy) NSString *imgurl;
@property (nonatomic ,copy) NSString *title;
@end

//收藏文章
@interface Article  : ARModel

@property (nonatomic ,copy) NSString *id;
@property (nonatomic ,copy) NSString *title;
@property (nonatomic ,copy) NSString *cover_url;
@property (nonatomic ,copy) NSString *weekend;
@property (nonatomic ,copy) NSString *category_id;
@property (nonatomic ,copy) NSString *content;
@property (nonatomic ,copy) NSString *created_at;
@property (nonatomic ,copy) NSString *updated_at;
@property (nonatomic ,copy) NSString *image_url;
@property (nonatomic ,copy) NSString *radio_url;
@property (nonatomic ,copy) NSString *cityid;
@property (nonatomic ,copy) NSString *is_tuijian;
@property (nonatomic ,copy) NSString *time;
@property (nonatomic ,copy) NSString *mp4;
@property (nonatomic ,copy) NSString *info;




@end


@interface TelNav : ARModel

@property (nonatomic ,copy) NSString *id;
@property (nonatomic ,copy) NSString *logo;
@property (nonatomic ,copy) NSString *name;
@property (nonatomic ,copy) NSString *pinyin;
@end

//分类
@interface CateModel : ARModel

@property (nonatomic ,copy) NSString *id;
@property (nonatomic ,copy) NSString *name;
@property (nonatomic ,copy) NSString *address;
@property (nonatomic ,copy) NSString *jingweidu;
@property (nonatomic ,copy) NSString *logo;
@property (strong, nonatomic)NSMutableArray *lxtel;
@property (nonatomic ,copy) NSString *pinyin;
//subCate(正定记忆 进入)
@property (nonatomic ,copy) NSString *flag;
@property (nonatomic ,copy) NSString *created_at;
@end

@interface LunList : ARModel

@property (nonatomic ,copy) NSString *id;
@property (nonatomic ,copy) NSString *name;
@property (nonatomic ,copy) NSString *created_at;
@property (nonatomic ,copy) NSString *logo;

@end




