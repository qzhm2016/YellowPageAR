//
//  HTTPManager.h
//  ARYellowPage
//
//  Created by youdian on 2018/4/17.
//  Copyright © 2018年 YouDianAd. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>
typedef void(^successBlock)(id response);
typedef void(^failBlock)(NSURLSessionDataTask *  task,NSError * error);
@interface HTTPManager : NSObject

/**
 单利句柄
 */
+(HTTPManager *)httpManager;


+(void)postDataWithPath:(NSString *)urlStr andParameters:(NSDictionary *)parameters success:(successBlock)success fail:(failBlock)fail;

+(void)getDataWithPath:(NSString *)urlStr andParameters:(NSDictionary *)parameters success:(successBlock)success fail:(failBlock)fail;

/**
 用户登录
 @ param path 请求路径
 @ param phone 用户手机
 @ param password 用户密码
 @ param completion 完成请求返回Block
 @ param fail 失败返回
 @ return
 */
-(void)loginWithPath:(NSString *)path userPhone:(NSString *)phone password:(NSString *)password completion:(successBlock)completion failure:(failBlock)fail;

/**
 获取短信验证码
 @ param phone 用户手机号
 @ param code 图片验证码
 @ return
 */
-(void)getSmsCode:(NSString *)path userPhone:(NSString *)phone imgNumber:(NSString *)code type:(NSString *)type completion:(successBlock)completion failure:(failBlock)fail;
/**
 用户注册
 @ param phone 用户手机
 @ param password 用户密码
 @ param code 短信验证码
 @ return
 */
-(void)userRegisterWithPath:(NSString *)path userPhone:(NSString *)phone password:(NSString *)password code:(NSString *)code completion:(successBlock)completion failure:(failBlock)fail;
/**
 用户修改密码
 @ param path 接口路径
 @ param phone 用户手机号
 @ param old_password 原密码
 @ param password 新密码
 @ param code 验证码
 @ return
 */
-(void)userChangePasswordWithPath:(NSString *)path userPhone:(NSString *)phone old_pwd:(NSString *)old_password newPwd:(NSString *)password code:(NSString *)code completion:(successBlock)completion failure:(failBlock)fail;

/**
 用户收藏列表
 @ param Authorization token
 @ return
 */

-(void)getArticleCollectPath:(NSString *)path token:(NSString *)token completion:(successBlock)completion failure:(failBlock)fail;


/**
 刷新用户token
 @ param
 @ return
 */
-(void)refreshUserTokenWithPath:(NSString *)path old_token:(NSString *)token completion:(successBlock)completion failure:(failBlock)fail;

/**
 定位获取城市id
 @ param
 @ return
 */

-(void)getCityidWithLocationPath:(NSString *)path location:(CLLocation *)location completion:(successBlock)completion failure:(failBlock)fail;
/**
 获取首页轮播图
 @ param
 @ return
 */
-(void)getMainBannerWithPath:(NSString *)path cityid:(NSString *)cityid completion:(successBlock)completion failure:(failBlock)fail;

/**
 获取分类内容
 @ param path 路径
 @ param cateid 分类id
 @ param page 分页 index
 @ return
 */

-(void)getListWithPath:(NSString *)path cateid:(NSString *)cateid andPage:(NSString *)page completion:(successBlock)completion failure:(failBlock)fail;

/**
 轮播图点击List
 @ param cityid 城市id
 @ param page 页数index
 @ param num没页返回数量
 @ return
 */
-(void)getLunListWithPath:(NSString *)path cityid:(NSString *)cityid andPage:(NSString *)page defaultNum:(NSString *)num completion:(successBlock)completion failure:(failBlock)fail;

/**
 进去新闻详情页
 @ param path 路径
 @ param aryid 文章id
 @ return
 */
-(void)getDetailNews:(NSString *)path artcleId:(NSString *)artid completion:(successBlock)completion failure:(failBlock)fail;


/**
 首页瀑布流点击进入分类详情
 @ param
 @ return
 */

-(void)getSubCatesWithPath:(NSString *)path cateid:(NSString *)cate_id completion:(successBlock)completion failure:(failBlock)fail;

/**
 首页最新文章 5
 @ param
 @ return
 */
-(void)getNewArticleWithPath:(NSString *)path cityid:(NSString *)cityid completion:(successBlock)completion failure:(failBlock)fail;

/**
 搜索文章
 @ param
 @ return
 */
-(void)getSerachReslutsWithPath:(NSString *)path cityid:(NSString *)cityid searchKey:(NSString *)key completion:(successBlock)completion failure:(failBlock)fail;
/**
 黄页导航搜索
 @ param
 @ return
 */
-(void)getNavSerachReslutsWithPath:(NSString *)path cityid:(NSString *)cityid  searchKey:(NSString *)key isPhone:(NSString *)flag completion:(successBlock)completion failure:(failBlock)fail;


/**
 用户收藏文章
 @ param path 路径
 @ param token 用户token
 @ param artid 文章id
 @ param status 文章当前状态
 @ return
 */
-(void)getArticleCollectWithPathAndToken:(NSString *)path token:(NSString *)token articleid:(NSString *)artid currentStatus:(NSString *)status completion:(successBlock)completion failure:(failBlock)fail;
/**
 AR扫描获取文章
 @ param path 请求路径
 @ param 
 @ return
 */
-(void)getArticleApparticlesWithPath:(NSString *)path page:(NSString *)page completion:(successBlock)completion failure:(failBlock)fail;


@end
