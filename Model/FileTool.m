//
//  FileTool.m
//  XiaoMenTong
//
//  Created by Mac on 2017/8/11.
//  Copyright © 2017年 BeiJingXiaoMenTong. All rights reserved.
//

#import "FileTool.h"

@implementation FileTool




+(NSString *)docmentPath{
    NSString *path=[[NSString alloc]init];
    path=[NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    return path;
}


+(NSString *)cachePath{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES);
    NSString *path = [paths objectAtIndex:0];
    return path;
}


+(NSString *)libraryPath{
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSLibraryDirectory, NSUserDomainMask, YES);
    NSString *path = [paths objectAtIndex:0];
    return path;
    
}

+(void)uploadOfflineData {
    NSString *filePath = [[self docmentPath] stringByAppendingPathComponent:@"syncData.txt"];
    NSFileManager *fileManager = [NSFileManager defaultManager];
    BOOL isExist = [fileManager fileExistsAtPath:filePath];
    if (isExist) {
        DLog(@"存在文件");
    }else{
        DLog(@"文件不存在");
    }
    
}

+(void)deleteFileAtFilePath:(NSString *)path {
    NSFileManager *fileManager = [NSFileManager defaultManager];
    BOOL res = [fileManager removeItemAtPath:path error:nil];
    if (res) {
        DLog(@"文件删除成功");
    }
    else{
        DLog(@"文件是否存在: %@",[fileManager isExecutableFileAtPath:path]?@"YES":@"NO");
    }
    
}




@end
