//
//  HTTPManager.m
//  ARYellowPage
//
//  Created by youdian on 2018/4/17.
//  Copyright © 2018年 YouDianAd. All rights reserved.
//

#import "HTTPManager.h"
static HTTPManager*manager = nil;


@implementation HTTPManager

+(instancetype)allocWithZone:(struct _NSZone *)zone {
    static dispatch_once_t one;
    dispatch_once(&one, ^{
        if (!manager) {
            manager = [super allocWithZone:zone];
        }
    });
    return manager;
}
+(HTTPManager *)httpManager{
    return [[self alloc]init];
}

+(void)postDataWithPath:(NSString *)urlStr andParameters:(NSDictionary *)parameters success:(successBlock)success fail:(failBlock)fail {
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    manager.requestSerializer.timeoutInterval = 10.0f;
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"application/json", @"text/json", @"text/javascript",@"application/x-json",@"text/html", nil];

    DLog(@"url =%@,parameters = %@",urlStr,parameters);
    [manager POST:urlStr parameters:parameters progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        success(responseObject);
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        fail( task, error);
    }];
}


+(void)getDataWithPath:(NSString *)urlStr andParameters:(NSDictionary *)parameters success:(successBlock)success fail:(failBlock)fail{
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    manager.requestSerializer.timeoutInterval = 10.0f;
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"application/json", @"text/json", @"text/javascript",@"application/x-json",@"text/html", nil];
    [manager GET:urlStr parameters:nil progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        success(responseObject);
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        fail( task, error);
    }];
}
//用户登录
-(void)loginWithPath:(NSString *)path userPhone:(NSString *)phone password:(NSString *)password completion:(successBlock)completion failure:(failBlock)fail{
    NSDictionary *parameters = NSDictionaryOfVariableBindings(phone,password);
    NSString *urlStr = [NSString stringWithFormat:@"%@%@",HOST,path];
    [HTTPManager postDataWithPath:urlStr andParameters:parameters success:completion fail:fail];
}
//获取验证码
-(void)getSmsCode:(NSString *)path userPhone:(NSString *)phone imgNumber:(NSString *)code type:(NSString *)type completion:(successBlock)completion failure:(failBlock)fail{
     NSDictionary *parameters = NSDictionaryOfVariableBindings(phone,code,type);
     NSString *urlStr = [NSString stringWithFormat:@"%@%@",HOST,path];
      [HTTPManager postDataWithPath:urlStr andParameters:parameters success:completion fail:fail];
}
//用户注册
-(void)userRegisterWithPath:(NSString *)path userPhone:(NSString *)phone password:(NSString *)password code:(NSString *)code completion:(successBlock)completion failure:(failBlock)fail{
    NSDictionary *parameters = NSDictionaryOfVariableBindings(phone,code,password);
    NSString *urlStr = [NSString stringWithFormat:@"%@%@",HOST,path];
    [HTTPManager postDataWithPath:urlStr andParameters:parameters success:completion fail:fail];
}
-(void)userChangePasswordWithPath:(NSString *)path userPhone:(NSString *)phone old_pwd:(NSString *)old_password newPwd:(NSString *)password code:(NSString *)code completion:(successBlock)completion failure:(failBlock)fail{
    NSDictionary *parameters = NSDictionaryOfVariableBindings(phone,code,password);
    NSString *urlStr = [NSString stringWithFormat:@"%@%@",HOST,path];
    [HTTPManager postDataWithPath:urlStr andParameters:parameters success:completion fail:fail];
}






//用户收藏列表
-(void)getArticleCollectPath:(NSString *)path token:(NSString *)token completion:(successBlock)completion failure:(failBlock)fail{
     NSString *urlStr = [NSString stringWithFormat:@"%@%@",HOST,path];
    DLog(@"url = %@",urlStr);
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    manager.requestSerializer.timeoutInterval = 30.0f;
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"application/json", @"text/json", @"text/javascript",@"application/x-json",@"text/html", nil];
    token = [NSString stringWithFormat:@"Bearer %@",token];
   [manager.requestSerializer setValue:token forHTTPHeaderField:@"Authorization"];
    [manager POST:urlStr parameters:nil progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        completion(responseObject);
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        fail(task, error);
    }];
   
}


//刷新用户token
-(void)refreshUserTokenWithPath:(NSString *)path old_token:(NSString *)token completion:(successBlock)completion failure:(failBlock)fail{
    NSString *urlStr = [NSString stringWithFormat:@"%@%@?token=%@",HOST,path,token];
    [HTTPManager postDataWithPath:urlStr andParameters:nil success:completion fail:fail];
}

-(void)getCityidWithLocationPath:(NSString *)path location:(CLLocation *)location completion:(successBlock)completion failure:(failBlock)fail{
     NSString *urlStr = [NSString stringWithFormat:@"%@%@",HOST,path];
    NSString *loa;
    if (location) {
      loa = [NSString stringWithFormat:@"%f,%f",location.coordinate.longitude,location.coordinate.latitude];
    }else{
        loa = @"114.515043,38.050947";
    }
#if TARGET_IPHONE_SIMULATOR
 loa = @"114.515043,38.050947";;
#endif
    NSDictionary *parameters = NSDictionaryOfVariableBindings(loa);
     [HTTPManager postDataWithPath:urlStr andParameters:parameters success:completion fail:fail];
    
}

//获取轮播图
-(void)getMainBannerWithPath:(NSString *)path cityid:(NSString *)cityid completion:(successBlock)completion failure:(failBlock)fail{
    NSString *urlStr = [NSString stringWithFormat:@"%@%@",HOST,path];
    NSDictionary *parameters = NSDictionaryOfVariableBindings(cityid);
     [HTTPManager postDataWithPath:urlStr andParameters:parameters success:completion fail:fail];
}

-(void)getListWithPath:(NSString *)path cateid:(NSString *)cateid andPage:(NSString *)page completion:(successBlock)completion failure:(failBlock)fail{
    NSString *urlStr = [NSString stringWithFormat:@"%@%@",HOST,path];
    NSDictionary *parameters = NSDictionaryOfVariableBindings(cateid,page);
    [HTTPManager postDataWithPath:urlStr andParameters:parameters success:completion fail:fail];
}

-(void)getLunListWithPath:(NSString *)path cityid:(NSString *)cityid andPage:(NSString *)page defaultNum:(NSString *)num completion:(successBlock)completion failure:(failBlock)fail{
    NSString *urlStr = [NSString stringWithFormat:@"%@%@",HOST,path];
    num = num.length>0?num:@"15";
    NSDictionary *parameters = NSDictionaryOfVariableBindings(cityid,page,num);
     [HTTPManager postDataWithPath:urlStr andParameters:parameters success:completion fail:fail];
}

-(void)getDetailNews:(NSString *)path artcleId:(NSString *)artid completion:(successBlock)completion failure:(failBlock)fail{
    NSDictionary *parameters ;
    NSString *urlStr = [NSString stringWithFormat:@"%@%@",HOST,path];
    NSString *token = [USER_DEFAULT objectForKey:@"token"];
    if (token.length>0) {
    parameters = NSDictionaryOfVariableBindings(artid,token);
    }else{
         parameters = NSDictionaryOfVariableBindings(artid);
    }
      [HTTPManager postDataWithPath:urlStr andParameters:parameters success:completion fail:fail];
}
//点击类似 正定记忆
-(void)getSubCatesWithPath:(NSString *)path cateid:(NSString *)cate_id completion:(successBlock)completion failure:(failBlock)fail {
    NSString *urlStr = [NSString stringWithFormat:@"%@%@",HOST,path];
    
    NSDictionary *parameters = @{@"id":cate_id};
     [HTTPManager postDataWithPath:urlStr andParameters:parameters success:completion fail:fail];
}

-(void)getNewArticleWithPath:(NSString *)path cityid:(NSString *)cityid completion:(successBlock)completion failure:(failBlock)fail{
    NSString *urlStr = [NSString stringWithFormat:@"%@%@",HOST,path];
    DLog(@"urlStr = %@",urlStr);
    NSDictionary *parameters = NSDictionaryOfVariableBindings(cityid);
    [HTTPManager postDataWithPath:urlStr andParameters:parameters success:completion fail:fail];
}

-(void)getSerachReslutsWithPath:(NSString *)path cityid:(NSString *)cityid searchKey:(NSString *)key completion:(successBlock)completion failure:(failBlock)fail{
    NSString *urlStr = [NSString stringWithFormat:@"%@%@",HOST,path];
    NSDictionary *parameters = NSDictionaryOfVariableBindings(cityid,key);
    [HTTPManager postDataWithPath:urlStr andParameters:parameters success:completion fail:fail];
}

-(void)getNavSerachReslutsWithPath:(NSString *)path cityid:(NSString *)cityid searchKey:(NSString *)key isPhone:(NSString *)flag completion:(successBlock)completion failure:(failBlock)fail{
    NSString *urlStr = [NSString stringWithFormat:@"%@%@",HOST,path];
    DLog(@"url = %@",urlStr);
    NSDictionary *parameters = NSDictionaryOfVariableBindings(cityid,key,flag);
    [HTTPManager postDataWithPath:urlStr andParameters:parameters success:completion fail:fail];
}

//收藏文章状态改变
-(void)getArticleCollectWithPathAndToken:(NSString *)path token:(NSString *)token articleid:(NSString *)artid currentStatus:(NSString *)status completion:(successBlock)completion failure:(failBlock)fail{
    NSString *urlStr = [NSString stringWithFormat:@"%@%@?token=%@",HOST,path,token];
    NSDictionary *parameters = NSDictionaryOfVariableBindings(artid,status);
     [HTTPManager postDataWithPath:urlStr andParameters:parameters success:completion fail:fail];
}
//获取AR扫描
-(void)getArticleApparticlesWithPath:(NSString *)path page:(NSString *)page completion:(successBlock)completion failure:(failBlock)fail{
    NSString *urlStr = [NSString stringWithFormat:@"%@%@",HOST,path];
    NSDictionary *parameters = NSDictionaryOfVariableBindings(page);
    [HTTPManager postDataWithPath:urlStr andParameters:parameters success:completion fail:fail];
}




@end
