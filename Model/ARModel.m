//
//  ARModel.m
//  ARYellowPage
//
//  Created by youdian on 2018/4/19.
//  Copyright © 2018年 YouDianAd. All rights reserved.
//

#import "ARModel.h"

@implementation ARModel

- (id)valueForUndefinedKey:(NSString *)key
{
    // subclass implementation should provide correct key value mappings for custom keys
    NSLog(@"%@：获取键值出错，未定义的键：%@", NSStringFromClass([self class]), key);
    return nil;
}

- (void)setValue:(id)value forUndefinedKey:(NSString *)key
{
    // subclass implementation should set the correct key value mappings for custom keys
    //  NSLog(@"%@：构造模型对象出错，未定义的键：%@", NSStringFromClass([self class]), key);
    
    DLog(@"未定义的键：%@",key);

}
@end

//轮播如
@implementation Banner



@end

//收藏文章
@implementation Article



@end

//黄页导航
@implementation TelNav



@end

@implementation CateModel

- (void)setValue:(id)value forKey:(NSString *)key {
    [super setValue:value forKey:key];
    if ([key isEqualToString:@"lxtel"]) {
        NSMutableArray *array = [NSMutableArray arrayWithCapacity:0];
        for (NSString *s  in value) {
            [array addObject:s ];
        }
        self.lxtel = array;
        
    }
    
}

@end


@implementation LunList



@end



