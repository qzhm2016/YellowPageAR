//
//  FileTool.h
//
//  Created by Mac on 2017/8/11.
//  Copyright © 2017年 BeiJingXiaoMenTong. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface FileTool : NSObject



+(NSString *)docmentPath;
+(NSString *)cachePath;
+(NSString *)libraryPath;

+(void)uploadOfflineData;
+(void)deleteFileAtFilePath:(NSString *)path;


@end
