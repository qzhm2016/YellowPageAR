//
//  NavSearchResultVC.m
//  ARYellowPage
//
//  Created by youdian on 2018/4/24.
//  Copyright © 2018年 YouDianAd. All rights reserved.
//

#import "NavSearchResultVC.h"
#import "UIImageView+WebCache.h"

@interface NavSearchResultVC ()<UITableViewDelegate,UITableViewDataSource,UITextFieldDelegate>
{
    NSInteger pageNum ;
    UILabel *telLabel;
    UILabel *addressLabel;
}
@end

@implementation NavSearchResultVC

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = TABLE_COLOR;
  
    [self loadTableView];
    [self getarticle];
    // Do any additional setup after loading the view.
}

-(void)loadTableView{
    _tableView = [[UITableView alloc]initWithFrame:self.view.bounds style:UITableViewStylePlain];
    _tableView.delegate = self;
    _tableView.dataSource = self;
     [_tableView  setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    _tableView.backgroundColor =TABLE_COLOR;
    [self.view addSubview:_tableView];
    _tableView.tableFooterView = [[UIView alloc]init];
    [_tableView registerClass:[CateTableViewCell class] forCellReuseIdentifier:@"cateCell"];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return self.dataList.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    CateTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cateCell" forIndexPath:indexPath];
    UITapGestureRecognizer *telTap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(telTap:)];
    UITapGestureRecognizer *navTap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(navTap:)];
    CateModel *cate = self.dataList[indexPath.row];
    NSString *urlString = [cate.logo stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    [cell.logoView sd_setImageWithURL:[NSURL URLWithString:urlString]];
    cell.selectionStyle =UITableViewCellSelectionStyleNone;
    cell.nameLabel.text = cate.name;
    cell.addressLbel.text = [NSString stringWithFormat:@"地址:%@",cate.address];
    cell.telLabel.text = [NSString stringWithFormat:@"电话:%@",cate.lxtel[0]];
    telLabel = cell.nameLabel;
    telLabel.tag = indexPath.row;
    [telLabel addGestureRecognizer:telTap];
    addressLabel = cell.addressLbel;
    addressLabel.tag = indexPath.row;
    [addressLabel addGestureRecognizer:navTap];
    
    return cell;
}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 120.0f;
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 65.0f;
}
- (nullable UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    UIView *header = [[UIView alloc]init];
    header.bounds = CGRectMake(0, 0, SCREEN_WIDTH, 65);
    header.backgroundColor = THEME_COLOR;
    UITextField *textField = [[UITextField alloc]init];
    textField.borderStyle = UITextBorderStyleRoundedRect;
    textField.placeholder = @"搜索内容";
    textField.layer.cornerRadius = 20.0f;
    textField.clipsToBounds = YES;
    textField.clearButtonMode = UITextFieldViewModeAlways;
    textField.leftView = [[UIImageView alloc]initWithImage:IMAGE_NAME(@"icon_search_gray")];
    textField.leftViewMode =UITextFieldViewModeAlways;
    textField.delegate = self;
    [textField addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
    [header addSubview:textField];
    [textField mas_makeConstraints:^(MASConstraintMaker *make) {
        make.center.equalTo(header);
        make.size.mas_equalTo(CGSizeMake(SCREEN_WIDTH-40, 40));
    }];
    return header;
}


-(void)telTap:(UITapGestureRecognizer*)tapGestureRecognizer {
    UILabel *label = (UILabel *)tapGestureRecognizer.view;
    CateModel *cate = self.dataList[label.tag];
    NSString *telString = cate.lxtel[0];
    NSURL *telUrl = [NSURL URLWithString:[NSString stringWithFormat:@"tel:%@",telString]];
    [[UIApplication sharedApplication] openURL:telUrl];
}


-(void)navTap:(UITapGestureRecognizer*)tapGestureRecognizer {
    UILabel *label = (UILabel *)tapGestureRecognizer.view;
    CateModel *cate = self.dataList[label.tag];
    DLog(@"导航至%@",cate.address);
}
#pragma mark UITextField Delegate

- (void)textFieldDidChange:(UITextField *)textField {
    
}
- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    textField.returnKeyType = UIReturnKeySearch;
    return YES;
}
- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    if (textField.text.length<1) {
        WTOAST(@"请输入搜索内容");
    }else{
        self.key = textField.text;
        [self getarticle];
       
    }
    
    return YES;
    
}
-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event{
    [self.view endEditing:YES];
}
-(void)getarticle{
    WS(weakSelf)
    _dataList = [NSMutableArray arrayWithCapacity:0];
    HTTPManager *manager = [HTTPManager httpManager];
    [self showHUD];
    [manager getNavSerachReslutsWithPath:getarticlePath cityid:AppDel.cityid searchKey:self.key isPhone:@"phone" completion:^(id response) {
        DLog(@"Res = %@",response);
        [weakSelf hideHUD];
        if ([response[@"code"] integerValue]==0) {
            [response[@"data"] enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                CateModel *cate = [[CateModel alloc]init];
                [cate setValuesForKeysWithDictionary:obj];
                [weakSelf.dataList addObject:cate];
            }];
        }else{
            WTOAST(response[@"msg"]);
        }
        [weakSelf.tableView reloadData];
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        WTOAST(@"网络连接错误");
        [weakSelf hideHUD];
    }];
}

-(void)viewWillAppear:(BOOL)animated{
     self.navigationItem.title = self.key;
    self.tabBarController.tabBar.hidden = YES;
}

-(void)viewWillDisappear:(BOOL)animated{
    self.navigationItem.title = @"";
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
