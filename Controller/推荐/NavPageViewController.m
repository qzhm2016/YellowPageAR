//
//  NavPageViewController.m
//  ARYellowPage
//
//  Created by youdian on 2018/4/16.
//  Copyright © 2018年 YouDianAd. All rights reserved.
//

#import "NavPageViewController.h"
#import "UIImageView+WebCache.h"
#import "NavCateViewController.h"
#import "NavSearchResultVC.h"


@interface NavPageViewController ()<UICollectionViewDelegate,UICollectionViewDataSource,UITextFieldDelegate>

@end

@implementation NavPageViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = TABLE_COLOR;
    
    
    [self searchHeader];
    [self loadNavCollectionView];
    [self requestTelcates];
    // Do any additional setup after loading the view.
}

-(void)loadNavCollectionView{
    UICollectionViewFlowLayout *flowLayout = [[UICollectionViewFlowLayout alloc]init];
    flowLayout.minimumLineSpacing = 10.0f;
    flowLayout.minimumInteritemSpacing = 10.0f;
    flowLayout.sectionInset = UIEdgeInsetsMake(20, 10, 10, 10);
    CGFloat itemWidth = (SCREEN_WIDTH-30)/2;
    flowLayout.itemSize = CGSizeMake(itemWidth, itemWidth);
    flowLayout.scrollDirection = UICollectionViewScrollDirectionVertical;
    _collectionView = [[UICollectionView alloc]initWithFrame:CGRectMake(0, 64+65, SCREEN_WIDTH, SCREEN_HEIGHT-64-65) collectionViewLayout:flowLayout];
    _collectionView.delegate = self;
    _collectionView.dataSource = self;
    _collectionView.backgroundColor = TABLE_COLOR;
    _collectionView.scrollEnabled = YES;
    [self.view addSubview:_collectionView];
    [_collectionView registerClass:[NavCollectionViewCell class] forCellWithReuseIdentifier:@"telnav"];
    
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return _dataList.count;
}
- ( UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    TelNav *t = self.dataList[indexPath.row];
    NavCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"telnav" forIndexPath:indexPath];
    DLog(@"logo=%@",t.logo);
    NSString *urlString = [t.logo stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    [cell.logoView sd_setImageWithURL:[NSURL URLWithString:urlString]];
    cell.titleLabel.text = t.name;
    return cell;
}


- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    
    [self.view endEditing:YES];
    TelNav *t = _dataList[indexPath.row];
    NavCateViewController *cate = [NavCateViewController new];
     cate.telNav = t;
    [self.navigationController pushViewController:cate animated:YES];
}


-(void)searchHeader{
    UIView *header = [[UIView alloc]init];
    header.frame = CGRectMake(0, 64, SCREEN_WIDTH, 65);
    header.backgroundColor = THEME_COLOR;
    [self.view addSubview:header];
    UITextField *textField = [[UITextField alloc]init];
    textField.borderStyle = UITextBorderStyleRoundedRect;
    textField.placeholder = @"搜索内容";
    textField.layer.cornerRadius = 20.0f;
    textField.clipsToBounds = YES;
    textField.clearButtonMode = UITextFieldViewModeAlways;
    textField.leftView = [[UIImageView alloc]initWithImage:IMAGE_NAME(@"icon_search_gray")];
    textField.leftViewMode =UITextFieldViewModeAlways;
    textField.delegate = self;
    [textField addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
    [header addSubview:textField];
    [textField mas_makeConstraints:^(MASConstraintMaker *make) {
        make.center.equalTo(header);
        make.size.mas_equalTo(CGSizeMake(SCREEN_WIDTH-40, 40));
    }];
}



#pragma mark UITextField Delegate

- (void)textFieldDidChange:(UITextField *)textField {
    
}
- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    textField.returnKeyType = UIReturnKeySearch;
    return YES;
}
- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    if (textField.text.length<1) {
        WTOAST(@"请输入搜索内容");
    }else{
       NavSearchResultVC *result = [NavSearchResultVC new];
        result.key = textField.text;
        [self.navigationController pushViewController:result animated:YES];
    }
    
    return YES;
    
}
-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event{
    [self.view endEditing:YES];
}


-(void)requestTelcates{
    WS(weakSelf)
    _dataList =[NSMutableArray arrayWithCapacity:0];
    [self.manager getMainBannerWithPath:telcatesPath cityid:AppDel.cityid completion:^(id response) {
        [response[@"data"] enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            TelNav *telNav = [[TelNav alloc]init];
            [telNav setValuesForKeysWithDictionary:obj];
            [weakSelf.dataList addObject:telNav];
        }];
        [weakSelf.collectionView reloadData];
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        
    }];
    
    
    
}

-(void)viewWillAppear:(BOOL)animated{
    self.navigationItem.title = @"黄页导航";
    self.tabBarController.tabBar.hidden = NO;
}
-(void)viewWillDisappear:(BOOL)animated{
    self.navigationItem.title = @"";
}

-(HTTPManager *)manager{
    if (!_manager) {
        _manager = [HTTPManager httpManager];
    }
    return _manager;
}
- (void)didReceiveMemoryWarning {
    
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
