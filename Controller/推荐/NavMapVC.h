//
//  NavMapVC.h
//  ARYellowPage
//
//  Created by youdian on 2018/4/27.
//  Copyright © 2018年 YouDianAd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AMapLocationKit/AMapLocationKit.h>
#import <AMapNaviKit/AMapNaviKit.h>
#import <MAMapKit/MAMapKit.h>
#import <iflyMSC/iflyMSC.h>

@interface NavMapVC : UIViewController<AMapNaviDriveViewDelegate,AMapNaviDriveManagerDelegate,MAMapViewDelegate,IFlySpeechSynthesizerDelegate>

@property (strong, nonatomic)CateModel *cate;

@property(nonatomic, strong)AMapNaviPoint * startPoint;
@property(nonatomic, strong)AMapNaviPoint * endPoint;

@property(nonatomic, strong)MAMapView * mapView;

@property(nonatomic, strong)AMapNaviDriveView  * driveView;
@property(nonatomic, strong)AMapNaviDriveManager * manager;
@property (strong, nonatomic)AMapLocationManager *locationManager;
@property (nonatomic, strong) IFlySpeechSynthesizer *iFlySpeechSynthesizer;
@end
