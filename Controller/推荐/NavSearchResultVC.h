//
//  NavSearchResultVC.h
//  ARYellowPage
//
//  Created by youdian on 2018/4/24.
//  Copyright © 2018年 YouDianAd. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NavSearchResultVC : UIViewController

@property (strong, nonatomic)UITableView *tableView;
@property (strong, nonatomic)NSMutableArray *dataList;
@property (strong, nonatomic)TelNav *telNav;
@property (nonatomic ,copy) NSString *key;
@end
