//
//  NavMapVC.m
//  ARYellowPage
//
//  Created by youdian on 2018/4/27.
//  Copyright © 2018年 YouDianAd. All rights reserved.
//

#import "NavMapVC.h"

@interface NavMapVC ()

@end

@implementation NavMapVC

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor whiteColor];
      [self iFlySettings];
    //初始化AMapNaviDriveManager
    [[AMapNaviDriveManager sharedInstance] setDelegate:self];
    
    //初始化AMapNaviDriveView
    if (self.driveView == nil)
    {
        self.driveView = [[AMapNaviDriveView alloc] initWithFrame:self.view.bounds];
        [self.driveView setDelegate:self];
    }
    
    //将AMapNaviManager与AMapNaviDriveView关联起来
    [[AMapNaviDriveManager sharedInstance] addDataRepresentative:self.driveView];
    //将AManNaviDriveView显示出来
    [self.view addSubview:self.driveView];
    

    // Do any additional setup after loading the view.
}

-(void)viewDidAppear:(BOOL)animated{
    [self showHUDWithMessage:@"路径规划中"];
    [super viewDidAppear:animated];
    [self getLocation];
    
}

-(void)getLocation {
    self.locationManager = [[AMapLocationManager alloc]init];
    // 带逆地理信息的一次定位（返回坐标和地址信息）
    [self.locationManager setDesiredAccuracy:kCLLocationAccuracyHundredMeters];
    //   定位超时时间，最低2s，此处设置为2s
    self.locationManager.locationTimeout =10;
    //   逆地理请求超时时间，最低2s，此处设置为2s
    self.locationManager.reGeocodeTimeout = 10;
    // 带逆地理（返回坐标和地址信息）。将下面代码中的 YES 改成 NO ，则不会返回地址信息。
    WS(weakSelf)
    [self.locationManager requestLocationWithReGeocode:NO completionBlock:^(CLLocation *location, AMapLocationReGeocode *regeocode, NSError *error) {
        if (error){
            if (error.code == AMapLocationErrorLocateFailed){
                WTOAST(@"定位错误");
                return;
            }
        }
        NSArray *array = [weakSelf.cate.jingweidu componentsSeparatedByString:@","];
        CGFloat longitude =[[array firstObject] floatValue];
         CGFloat latitude =[[array lastObject] floatValue];
        self.startPoint = [AMapNaviPoint locationWithLatitude:location.coordinate.latitude longitude:location.coordinate.longitude];
        self.endPoint   = [AMapNaviPoint locationWithLatitude:latitude longitude:longitude];
        //路径规划
        [[AMapNaviDriveManager sharedInstance]  calculateDriveRouteWithStartPoints:@[self.startPoint]
                                                                         endPoints:@[self.endPoint]
                                                                         wayPoints:nil
                                                                   drivingStrategy:AMapNaviDrivingStrategySingleDefault];
       
    }];
    
}

//路径规划成功后，开始模拟导航
- (void)driveManagerOnCalculateRouteSuccess:(AMapNaviDriveManager *)driveManager{
      [self hideHUD];
#ifdef DEBUG
 [[AMapNaviDriveManager sharedInstance] startEmulatorNavi];
#else
  [[AMapNaviDriveManager sharedInstance] startGPSNavi];
#endif
    
 
}

- (void)driveViewCloseButtonClicked:(AMapNaviDriveView *)driveView{
    
    [self.navigationController popViewControllerAnimated:YES];
}
- (BOOL)driveManagerIsNaviSoundPlaying:(AMapNaviDriveManager *)driveManager
{
    return YES;
}

- (void)driveManager:(AMapNaviDriveManager *)driveManager playNaviSoundString:(NSString *)soundString soundStringType:(AMapNaviSoundType)soundStringType
{
    NSLog(@"sounString = %@",soundString);
    [_iFlySpeechSynthesizer startSpeaking:soundString];
    
}


-(void)iFlySettings{
    //Appid是应用的身份信息，具有唯一性，初始化时必须要传入Appid。
    NSString *initString = [[NSString alloc] initWithFormat:@"appid=%@", @"5ae2b9b3"];
    [IFlySpeechUtility createUtility:initString];
    
    //获取语音合成单例
    _iFlySpeechSynthesizer = [IFlySpeechSynthesizer sharedInstance];
    //设置协议委托对象
    _iFlySpeechSynthesizer.delegate = self;
    //设置合成参数
    //设置在线工作方式
    [_iFlySpeechSynthesizer setParameter:[IFlySpeechConstant TYPE_CLOUD]
                                  forKey:[IFlySpeechConstant ENGINE_TYPE]];
    //语速,取值范围 0~100
    [_iFlySpeechSynthesizer setParameter:@"50" forKey:[IFlySpeechConstant SPEED]];
    //设置音量，取值范围 0~100
    [_iFlySpeechSynthesizer setParameter:@"50"
                                  forKey: [IFlySpeechConstant VOLUME]];
    //发音人，默认为”xiaoyan”，可以设置的参数列表可参考“合成发音人列表”
    [_iFlySpeechSynthesizer setParameter:@"xiaoyan"
                                  forKey: [IFlySpeechConstant VOICE_NAME]];
    //音频采样率,目前支持的采样率有 16000 和 8000
    [_iFlySpeechSynthesizer setParameter:@"8000" forKey: [IFlySpeechConstant SAMPLE_RATE]];
    //保存合成文件名，如不再需要，设置为nil或者为空表示取消，默认目录位于library/cache下
    [_iFlySpeechSynthesizer setParameter:nil
                                  forKey: [IFlySpeechConstant TTS_AUDIO_PATH]];
    
    
}




- (void) onCompleted:(IFlySpeechError *) error {
    //合成结束
}

- (void) onSpeakBegin {
   //合成开始
}

- (void) onBufferProgress:(int) progress message:(NSString *)msg {
    //合成缓冲进度
}

- (void) onSpeakProgress:(int) progress beginPos:(int)beginPos endPos:(int)endPos {
   //合成播放进度
}
- (void)dealloc
{
    [[AMapNaviDriveManager sharedInstance] stopNavi];
    [[AMapNaviDriveManager sharedInstance] removeDataRepresentative:self.driveView];
    [[AMapNaviDriveManager sharedInstance] setDelegate:nil];
    [_iFlySpeechSynthesizer stopSpeaking];
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    self.navigationController.navigationBar.hidden = YES;
    
}
-(void)viewDidDisappear:(BOOL)animated{
    [super viewDidDisappear:animated];
     self.navigationController.navigationBar.hidden = NO;
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
