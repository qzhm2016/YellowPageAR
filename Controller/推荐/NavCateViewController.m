//
//  NavCateViewController.m
//  ARYellowPage
//
//  Created by youdian on 2018/4/19.
//  Copyright © 2018年 YouDianAd. All rights reserved.
//

#import "NavCateViewController.h"
#import "UIImageView+WebCache.h"
#import "MJRefresh.h"
#import "NavMapVC.h"




@interface NavCateViewController ()<UITableViewDelegate,UITableViewDataSource>
{
    NSInteger pageNum ;
    UILabel *telLabel;
    UILabel *addressLabel;
}
@end

@implementation NavCateViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    pageNum = 1;
    _dataList = [NSMutableArray arrayWithCapacity:0];
    
    self.view.backgroundColor = TABLE_COLOR;
    [self requestDataListWithPage];
    [self loadTableView];
    // Do any additional setup after loading the view.
}


-(void)loadTableView{
    _tableView = [[UITableView alloc]initWithFrame:self.view.bounds style:UITableViewStylePlain];
    _tableView.delegate = self;
     [_tableView  setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    _tableView.dataSource = self;
    _tableView.backgroundColor =TABLE_COLOR;
    //_tableView.tableHeaderView = imgView;
    [self.view addSubview:_tableView];
    _tableView.tableFooterView = [[UIView alloc]init];
    [_tableView registerClass:[CateTableViewCell class] forCellReuseIdentifier:@"cateCell"];
    WS(weakSelf)
//    MJRefreshNormalHeader *header = [MJRefreshNormalHeader  headerWithRefreshingBlock:^{
//        
//    }];
//   _tableView.mj_header = header;
//    header.lastUpdatedTimeLabel.hidden = NO;
    MJRefreshBackNormalFooter  *footer =[MJRefreshBackNormalFooter footerWithRefreshingBlock:^{
        self->pageNum++;
        [weakSelf requestDataListWithPage];
        
    }];
    _tableView.mj_footer = footer;
}



- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return self.dataList.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    CateTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cateCell" forIndexPath:indexPath];
     UITapGestureRecognizer *telTap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(telTap:)];
    UITapGestureRecognizer *navTap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(navTap:)];
    CateModel *cate = self.dataList[indexPath.row];
    NSString *urlString = [cate.logo stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    [cell.logoView sd_setImageWithURL:[NSURL URLWithString:urlString]];
    cell.selectionStyle =UITableViewCellSelectionStyleNone;
    cell.nameLabel.text = cate.name;
    cell.addressLbel.text = [NSString stringWithFormat:@"地址:%@",cate.address];
    cell.telLabel.text = [NSString stringWithFormat:@"电话:%@",cate.lxtel[0]];
    
    telLabel = cell.nameLabel;
     telLabel.tag = indexPath.row;
    [telLabel addGestureRecognizer:telTap];
    addressLabel = cell.addressLbel;
    addressLabel.tag = indexPath.row;
    [addressLabel addGestureRecognizer:navTap];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 120.0f;
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 0.0f;
}

-(void)telTap:(UITapGestureRecognizer*)tapGestureRecognizer {
    UILabel *label = (UILabel *)tapGestureRecognizer.view;
    CateModel *cate = self.dataList[label.tag];
    NSString *telString = cate.lxtel[0];
    NSURL *telUrl = [NSURL URLWithString:[NSString stringWithFormat:@"tel:%@",telString]];
    [[UIApplication sharedApplication] openURL:telUrl];
}


-(void)navTap:(UITapGestureRecognizer*)tapGestureRecognizer {
    [self showHUD];
    UILabel *label = (UILabel *)tapGestureRecognizer.view;
    CateModel *cate = self.dataList[label.tag];
    DLog(@"导航至%@",cate.jingweidu);
    NavMapVC *map = [NavMapVC new];
    map.cate = cate;
    [self hideHUD];
    [self.navigationController pushViewController:map animated:YES];
    
    
    
    
    
}

-(void)viewWillAppear:(BOOL)animated{
   self.navigationItem.title = self.telNav.name;
    self.tabBarController.tabBar.hidden = YES;
    self.navigationController.navigationBar.hidden = NO;
}
-(void)viewWillDisappear:(BOOL)animated{
    self.navigationItem.title = @"";
}


-(void)requestDataListWithPage{
    HTTPManager *manager = [HTTPManager httpManager];
    WS(weakSelf)
    NSString *page = [NSString stringWithFormat:@"%ld",pageNum];
    [manager getListWithPath:list2Path cateid:self.telNav.id andPage:page completion:^(id response) {
        [weakSelf.tableView.mj_header endRefreshing];
        [weakSelf.tableView.mj_footer endRefreshing];
        if ([response[@"code"] integerValue]==0) {
            [response[@"data"] enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                CateModel *cate = [[CateModel alloc]init];
                [cate setValuesForKeysWithDictionary:obj];
                [weakSelf.dataList addObject:cate];
            }];
        }else{
            WTOAST(response[@"msg"]);
        }
        [weakSelf.tableView reloadData];
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        WTOAST(@"请求失败");
        [weakSelf.tableView.mj_header endRefreshing];
        [weakSelf.tableView.mj_footer endRefreshing];
    }];
    
    
}




- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
