//
//  NavCateViewController.h
//  ARYellowPage
//
//  Created by youdian on 2018/4/19.
//  Copyright © 2018年 YouDianAd. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NavCateViewController : UIViewController

@property (strong, nonatomic)UITableView *tableView;
@property (strong, nonatomic)NSMutableArray *dataList;
@property (strong, nonatomic)TelNav *telNav;
@end
