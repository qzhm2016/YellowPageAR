//
//  SubCateViewController.m
//  ARYellowPage
//
//  Created by youdian on 2018/4/23.
//  Copyright © 2018年 YouDianAd. All rights reserved.
//

#import "SubCateViewController.h"
#import "UIImageView+WebCache.h"
#import "MJRefresh.h"
#import "DetailViewController.h"
#import "SearchResultVC.h"

@interface SubCateViewController ()<UITableViewDelegate,UITableViewDataSource,UITextFieldDelegate>
@property (assign, nonatomic)NSInteger indexPage;
@property (strong, nonatomic)UITableView *tableView;
@property (strong, nonatomic)NSMutableArray *dataList;
@end

@implementation SubCateViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = TABLE_COLOR;
    _dataList = [NSMutableArray arrayWithCapacity:0];
    [self loadTableView];
    [self requestSubCates];
    // Do any additional setup after loading the view.
}


-(void)loadTableView{
    _tableView = [[UITableView alloc]initWithFrame:self.view.bounds style:UITableViewStylePlain];
    _tableView.delegate = self;
     [_tableView  setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    _tableView.dataSource = self;
    [self.view addSubview:_tableView];
    _tableView.tableFooterView = [[UIView alloc]init];
    [_tableView registerClass:[LunListTableViewCell class] forCellReuseIdentifier:@"subCateCell"];
}

#pragma UITableViewDelegate&&DataSource

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return _dataList.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    LunListTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"subCateCell" forIndexPath:indexPath];
    CateModel *subCate = self.dataList[indexPath.row];
    NSString *urlString = [subCate.logo stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    [cell.logoView sd_setImageWithURL:[NSURL URLWithString:urlString]];
    cell.titleLabel.text = subCate.name;
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    CateModel *subCate = self.dataList[indexPath.row];
    if ([subCate.flag isEqualToString:@"subcate"]) {
        SearchResultVC *search = [SearchResultVC new];
        search.artcleid = subCate.id;
        search.navigationItem.title = subCate.name;
        [self.navigationController pushViewController:search animated:YES];
    }else{
        DetailViewController *detail = [DetailViewController new];
        detail.artid = subCate.id;
        detail.artTitle = subCate.name;
        [self.navigationController pushViewController:detail animated:YES];
    }
    
    
    
   
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 180.0f;
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 65.0f;
}
- (nullable UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    UIView *header = [[UIView alloc]init];
    header.bounds = CGRectMake(0, 0, SCREEN_WIDTH, 65);
    header.backgroundColor = THEME_COLOR;
    UITextField *textField = [[UITextField alloc]init];
    textField.borderStyle = UITextBorderStyleRoundedRect;
    textField.placeholder = @"搜索内容";
    textField.layer.cornerRadius = 20.0f;
    textField.clipsToBounds = YES;
    textField.clearButtonMode = UITextFieldViewModeAlways;
    textField.leftView = [[UIImageView alloc]initWithImage:IMAGE_NAME(@"icon_search_gray")];
    textField.leftViewMode =UITextFieldViewModeAlways;
    textField.delegate = self;
    [textField addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
    [header addSubview:textField];
    [textField mas_makeConstraints:^(MASConstraintMaker *make) {
        make.center.equalTo(header);
        make.size.mas_equalTo(CGSizeMake(SCREEN_WIDTH-40, 40));
    }];
    return header;
}

#pragma mark UITextField Delegate

- (void)textFieldDidChange:(UITextField *)textField {
    
}
- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    textField.returnKeyType = UIReturnKeySearch;
    
    return YES;
}
- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    if (textField.text.length<1) {
        WTOAST(@"请输入搜索内容");
    }else{
        SearchResultVC *result = [SearchResultVC new];
        result.key = textField.text;
        [self.navigationController pushViewController:result animated:YES];
    }
    
    
    
    
    return YES;
    
}
-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event{
    [self.view endEditing:YES];
}


-(void)requestSubCates{
    WS(weakSelf)
    HTTPManager *manager = [HTTPManager httpManager];
    [self showHUD];
    [manager getSubCatesWithPath:getsubcatesPath cateid:self.subCate.id completion:^(id response) {
        [weakSelf hideHUD];
        if ([response[@"code"] integerValue]==0) {
            [response[@"data"] enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
               CateModel *subCate = [[CateModel alloc]init];
                [subCate setValuesForKeysWithDictionary:obj];
                [weakSelf.dataList addObject:subCate];
            }];
            [weakSelf.tableView reloadData];
        }else{
            WTOAST(response[@"msg"]);
        }
        
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        [weakSelf hideHUD];
        WTOAST(@"请求失败");
    }];
    
    
    
    
    
}


-(void)viewWillAppear:(BOOL)animated{
    self.navigationItem.title = self.subCate.name;
    self.tabBarController.tabBar.hidden = YES;
}
-(void)viewWillDisappear:(BOOL)animated{
    self.navigationItem.title = @"";
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
