//
//  DetailViewController.m
//  ARYellowPage
//
//  Created by youdian on 2018/4/20.
//  Copyright © 2018年 YouDianAd. All rights reserved.
//

#import "DetailViewController.h"
#import <AVFoundation/AVFoundation.h>

#import <UMCommon/UMCommon.h>
#import <UShareUI/UShareUI.h>


@interface DetailViewController ()<AVSpeechSynthesizerDelegate,UIWebViewDelegate,WKNavigationDelegate>

@property (nonatomic, strong) UIProgressView *progressView;
@property (nonatomic, strong)WKWebView *webView;
@property (nonatomic ,copy) NSString *htmlString;


@property (nonatomic, strong) AVSpeechSynthesizer *synthesizer;
@property (nonatomic, strong)  AVSpeechUtterance * utterance;
@property (nonatomic, strong) AVSpeechSynthesisVoice*voice;
@property (nonatomic, assign)BOOL isPlay;
@property (nonatomic, assign)BOOL isPause;
@end

@implementation DetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor whiteColor];
    self.navigationItem.title = self.artTitle;
   [self getArtcleDetail];
    
    
    
    
    // Do any additional setup after loading the view.
}

-(void)addButtons{
    WS(weakSelf)
    NSArray *nameArray;
    if ([self.status integerValue]==0) {
        nameArray= @[@"back_main_iv",@"article_share_iv",@"voice_play_iv",@"collect_article_no"];
    }else {
         nameArray = @[@"back_main_iv",@"article_share_iv",@"voice_play_iv",@"collect_article_yes"];
    }
    if (self.urlPath.length>0) {
         nameArray = @[@"back_main_iv",@"article_share_iv",@"voice_play_iv"];
    }
    
    
    
    for (int i = 0; i< nameArray.count; i++) {
        UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
        button.backgroundColor = THEME_COLOR;
        button.layer.cornerRadius = 22.5f;
        [button setImage:IMAGE_NAME(nameArray[i]) forState:UIControlStateNormal];
        button.imageEdgeInsets = UIEdgeInsetsMake(10, 10, 10, 10);
        button.tag = 10+i;
        [button addTarget:self action:@selector(btnClick:) forControlEvents:UIControlEventTouchUpInside];
        [self.view addSubview:button];
        [self.view bringSubviewToFront:button];
        CGFloat space = nameArray.count>3?175:257;
        [button mas_makeConstraints:^(MASConstraintMaker *make) {
            if (i<1) {
                 make.left.equalTo(weakSelf.view).offset(20);
            }else{
                make.left.equalTo(weakSelf.view).offset(space+65*(i-1));
            }
            make.bottom.equalTo(weakSelf.view).offset(-120);
            make.size.mas_equalTo(CGSizeMake(45, 45));
        }];
    }
    
    _collectButton = (UIButton *)[self.view viewWithTag:13];
    
}

-(void)btnClick:(UIButton *)button{
    switch (button.tag) {
        case 10:
            self.tabBarController.selectedIndex = AppDel.selectIndex;
            [self.navigationController popViewControllerAnimated:YES];
            break;
        case 13:
            [self addCollectArticle];
            break;
        case 11:
            [self share];
            break;
        case 12:
            [self playText];
            break;
            
        default:
            break;
    }
}

-(void)playText {
    
    NSString *string = [self filterHTML:self.detailArticle.content];
    _isPlay = !_isPlay;
    if (_isPlay) {
        if (_isPause) {
            [self.synthesizer continueSpeaking];
            _isPause = NO;
        }else{
             [self playVoiceWithText:string];
        }
    }else{
        [self.synthesizer pauseSpeakingAtBoundary:AVSpeechBoundaryWord];
        _isPause = YES;
    }
}
-(void)share {
    WS(weakSelf)
    [UMSocialUIManager showShareMenuViewInWindowWithPlatformSelectionBlock:^(UMSocialPlatformType platformType, NSDictionary *userInfo) {
        [weakSelf shareWebPageToPlatformType:platformType];
    }];
}


- (void)shareWebPageToPlatformType:(UMSocialPlatformType)platformType{
    //创建分享消息对象
    UMSocialMessageObject *messageObject = [UMSocialMessageObject messageObject];
    //创建网页内容对象
    NSString *title = self.detailArticle.title.length>0?self.detailArticle.title:@"AR黄页";
    NSString* thumbURL = [self.detailArticle.image_url stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    DLog(@"url = %@",thumbURL);
    UMShareWebpageObject *shareObject = [UMShareWebpageObject shareObjectWithTitle:title descr:title thumImage:thumbURL];
    //设置网页地址
     NSString *webShareUrl = @"https://arzding.zhyell.com/templates/jydetail.html?id=";
    NSString *url =[NSString stringWithFormat:@"%@%@&name=%@",webShareUrl,self.detailArticle.id,title];
    
   shareObject.webpageUrl = [url stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    //分享消息对象设置分享内容对象
    messageObject.shareObject = shareObject;
    //调用分享接口
    [[UMSocialManager defaultManager] shareToPlatform:platformType messageObject:messageObject currentViewController:self completion:^(id data, NSError *error) {
        DLog(@"data = %@",data);
        if (error) {
            
            UMSocialLogInfo(@"************Share fail with error %@*********",error);
        }else{
            if ([data isKindOfClass:[UMSocialShareResponse class]]) {
                UMSocialShareResponse *resp = data;
                //分享结果消息
               DLog(@"response message is %@",resp.message);
                //第三方原始返回的数据
                DLog(@"response originalResponse data is %@",resp.originalResponse);
            }else{
                DLog(@"response data is %@",data);
            }
        }
    }];
}



-(void)addCollectArticle{
    NSString *token = [USER_DEFAULT objectForKey:@"token"];
    if (token.length<1) {
        [AppDel alertLogin];
    }else{
        [self requestCollectArticle];
    }
    
    
    
    
}
-(void)loadHtmlWithHtmlString:(NSString *)htmlStr{
   _webView = [[WKWebView alloc] initWithFrame:CGRectMake(0, 64, SCREEN_WIDTH, SCREEN_HEIGHT-64)];
//    NSString *cache = [FileTool cachePath];
//    NSString *filePath = [cache stringByAppendingPathComponent:@"web.html"];
//    NSURL *fileUrl = [[NSURL alloc] initWithString:filePath];
     // 最后将webView添加到界面
   _webView.navigationDelegate = self;
    [self.view addSubview:_webView];
    [self.webView loadHTMLString:htmlStr baseURL:nil];
    
   /*
    NSString *urlString = [NSString stringWithFormat:@"file://%@",[[NSBundle mainBundle] pathForResource:@"web" ofType:@"html"]];
    NSURL *fileUrl = [[NSBundle mainBundle] URLForResource:@"web.html" withExtension:nil];
     2.创建请求
    NSMutableURLRequest *request =[NSMutableURLRequest requestWithURL:fileUrl];
     3.加载网页
    [_webView loadRequest:request];
    */
   

}

-(void)requestCollectArticle{
    WS(weakSelf)
    HTTPManager *manager = [HTTPManager httpManager];
    [self showHUD];
    NSString *token  = [USER_DEFAULT objectForKey:@"token"];
    [manager getArticleCollectWithPathAndToken:@"/api/article/getCollect" token:token articleid:self.detailArticle.id currentStatus:self.status completion:^(id response) {
        [weakSelf hideHUD];
        WTOAST(response[@"msg"]);
        if ([response[@"code"] integerValue]==0) {
            if ([weakSelf.status integerValue]==0) {
                weakSelf.status = @"1";//收藏状态改变
                 [weakSelf.collectButton setImage:IMAGE_NAME(@"collect_article_yes") forState:UIControlStateNormal];
            }else{
                weakSelf.status = @"0";//收藏状态改变
                [weakSelf.collectButton setImage:IMAGE_NAME(@"collect_article_no") forState:UIControlStateNormal];
            }
            [[NSNotificationCenter defaultCenter] postNotificationName:@"refresh" object:nil];
        }
        
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        [weakSelf hideHUD];
    }];
    
    
    
}
-(void)getArtcleDetail{
    WS(weakSelf)
    [self showHUDWithMessage:@"正在加载"];
    HTTPManager *manager = [HTTPManager httpManager];
    NSString *path = self.urlPath.length>0?self.urlPath:getDetailPath;
    [manager getDetailNews:path artcleId:self.artid completion:^(id response) {
        [weakSelf hideHUD];
        weakSelf.detailArticle = [[Article alloc]init];
        [weakSelf.detailArticle setValuesForKeysWithDictionary:response[@"data"]];
        weakSelf.status = response[@"status"];
        [weakSelf saveHtmlString: weakSelf.detailArticle];
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
         [weakSelf hideHUD];
    }];
}
-(void)saveHtmlString:(Article*)article{
    DLog(@"htmlString = %@",article.content);
    NSString *cache = [FileTool cachePath];
    NSMutableString *string = [NSMutableString stringWithCapacity:0];
    [string appendString:@"<meta charset=\"UTF-8\"><meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\">"];
    
    [string appendFormat:@"</p><p style=\"text-align: center;\"><img src=\"%@\" style=\"max-width:100%%;\"></p>",article.image_url];
    [string appendFormat:@"<h2>%@</h2>",article.title];
    [string appendFormat:@"<p>%@发布</p>",article.updated_at];
//    <video width="320" height="240" controls>
//    <source src="movie.mp4" type="video/mp4">
//    <source src="movie.ogg" type="video/ogg">
//    </video>
    if (self.detailArticle.mp4.length>0&&self.urlPath.length<1) {
          [string appendFormat:@"<video width=\100%%\" height=\"240\" controls poster = \"%@\"> <source src=\"%@\" type=\"video/mp4\"> </video>",self.detailArticle.image_url,self.detailArticle.mp4];
    }
    [string appendString:article.content];
    [string appendString:@"<p align=\"center\">--全文完--</p>"];
    [self loadHtmlWithHtmlString:string];
    NSString *filePath = [cache stringByAppendingPathComponent:@"web.html"];
    DLog(@"Path = %@",filePath);
    [string writeToFile:filePath atomically:YES encoding:NSUTF8StringEncoding error:nil];
    NSFileManager *fileManager = [NSFileManager defaultManager];
    BOOL isExist = [fileManager fileExistsAtPath:filePath];
    if (isExist) {
        DLog(@"保存成功");
    }else{
        DLog(@"保存字符失败");
        WTOAST(@"加载失败");
    }
}
//过滤标签
-(NSString *)filterHTML:(NSString *)html
{
    NSScanner * scanner = [NSScanner scannerWithString:html];
    NSString * text = nil;
    while([scanner isAtEnd]==NO)
    {
        [scanner scanUpToString:@"<" intoString:nil];
        [scanner scanUpToString:@">" intoString:&text];
        html = [html stringByReplacingOccurrencesOfString:[NSString stringWithFormat:@"%@>",text] withString:@""];
    }
    return html;
}

-(void)playVoiceWithText:(NSString *)text{
    _utterance = [[AVSpeechUtterance alloc]initWithString:text];
    _utterance.rate=0.5;// 设置语速，范围0-1，注意0最慢，1最快
    //_utterance.pitchMultiplier = 0.95f;//语调默认1
    _voice = [AVSpeechSynthesisVoice voiceWithLanguage:@"zh-CN"];//设置发音，这是中文普通话
    _utterance.voice= _voice;
    [self.synthesizer speakUtterance:_utterance];
}


-(AVSpeechSynthesizer *)synthesizer{
    if (!_synthesizer) {
        _synthesizer = [[AVSpeechSynthesizer alloc]init];
        _synthesizer.delegate = self;
    }
    return _synthesizer;
}


-(void)viewWillAppear:(BOOL)animated{
    self.tabBarController.tabBar.hidden = YES;
}
-(void)viewWillDisappear:(BOOL)animated{
    self.tabBarController.tabBar.hidden = NO;
    [self.synthesizer stopSpeakingAtBoundary:AVSpeechBoundaryWord];
}

- (void)webView:(WKWebView *)webView didStartProvisionalNavigation:(null_unspecified WKNavigation *)navigation{
     [self showHUDWithMessage:@"正在加载"];
}
- (void)webView:(WKWebView *)webView didFailProvisionalNavigation:(null_unspecified WKNavigation *)navigation withError:(NSError *)error;{
     WTOAST(@"加载失败");
    
}
- (void)webView:(WKWebView *)webView didFinishNavigation:(null_unspecified WKNavigation *)navigation{
    [self hideHUD];
    [self addButtons];
}



- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType{
    return YES;
}
- (void)webViewDidStartLoad:(UIWebView *)webView{
    [self showHUDWithMessage:@"正在加载"];
}
- (void)webViewDidFinishLoad:(UIWebView *)webView{
//    [webView stringByEvaluatingJavaScriptFromString:@"document.getElementsByTagName('body')[0].style.webkitTextSizeAdjust= '120%'"];
    [self hideHUD];
    [self addButtons];
}
- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error{
    WTOAST(@"加载失败");
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
