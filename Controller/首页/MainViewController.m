//
//  MainViewController.m
//  ARYellowPage
//
//  Created by youdian on 2018/4/16.
//  Copyright © 2018年 YouDianAd. All rights reserved.
//

#import "MainViewController.h"
#import "SDCycleScrollView.h"
#import "LunListViewController.h"
#import "UIImageView+WebCache.h"
#import "SubCateViewController.h"
#import "DetailViewController.h"


#import "ViewController.h"





@interface MainViewController ()<UICollectionViewDelegate,UICollectionViewDataSource,UITableViewDelegate,UITableViewDataSource, SDCycleScrollViewDelegate>

@property (strong, nonatomic)UIScrollView *scrollView;
@property (strong, nonatomic)SDCycleScrollView *cycleScrollView;
@property (strong, nonatomic)UICollectionView *collectionView;
@property (strong, nonatomic)UITableView *tableView;
@property (strong, nonatomic)NSMutableArray *dataArray;
@property (strong, nonatomic)NSMutableArray *dataList;
@property (strong, nonatomic)NSMutableArray *cateList;
@property (strong, nonatomic)NSMutableDictionary *cateData;
@property (strong, nonatomic)UIView *lastView;
@property (assign ,nonatomic)float collection_height;

@end

@implementation MainViewController

- (void)viewDidLoad {
    [super viewDidLoad];
     self.view.backgroundColor =TABLE_COLOR;
     self.navigationItem.title = @"AR黄页";
    [self locate];
    [self addScrollView];
    [self refeshToken];
    
    
    // Do any additional setup after loading the view.
}

-(void)refeshToken{
    HTTPManager *manager = [HTTPManager httpManager];
    NSString *token = [USER_DEFAULT objectForKey:@"token"];
    [manager refreshUserTokenWithPath:refreshPath old_token:token completion:^(id response) {
        DLog(@"Res = %@",response);
        if ([response[@"code"] integerValue ]==0) {
            [USER_DEFAULT setObject:response[@"data"] forKey:@"token"];
            [USER_DEFAULT synchronize];
        }
        
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        DLog(@"errorCode = %ld",error.code);
        if (error.code==-1011) {
            [USER_DEFAULT removeObjectForKey:@"token"];
            [USER_DEFAULT synchronize];
        }else{
            WTOAST(@"网络连接错误");
        }
    }];
}
-(void)addScrollView {
    _scrollView = [[UIScrollView alloc]initWithFrame:self.view.bounds];
    _scrollView.contentSize = CGSizeMake(SCREEN_WIDTH, 1450);
    _scrollView.showsHorizontalScrollIndicator= NO;
    _scrollView.showsVerticalScrollIndicator= NO;
    [self.view addSubview:_scrollView];
}
-(void)addBanner {
    CGRect rect = CGRectMake(0, 0, SCREEN_WIDTH, 225*PIX);
    NSMutableArray *imgUrls = [NSMutableArray arrayWithCapacity:0];
    
    for (Banner *b in self.bannerArray) {
        NSString *urlStr = [b.imgurl stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        [imgUrls addObject:urlStr];
    }
    _cycleScrollView = [SDCycleScrollView cycleScrollViewWithFrame:rect imageNamesGroup:imgUrls];
    _cycleScrollView.pageControlStyle = SDCycleScrollViewPageContolStyleClassic;
    _cycleScrollView.pageDotColor = TABLE_COLOR;
    _cycleScrollView.delegate = self;
    _cycleScrollView.currentPageDotColor = THEME_COLOR;
    _cycleScrollView.showPageControl = YES;
    [ _collectionView addSubview:_cycleScrollView];

}
/** 点击图片回调 */
- (void)cycleScrollView:(SDCycleScrollView *)cycleScrollView didSelectItemAtIndex:(NSInteger)index{
    LunListViewController *lun = [LunListViewController new];
    lun.path = lunlistPath;
    [self.navigationController pushViewController:lun animated:YES];
}

-(void)loadCollection {
    UICollectionViewFlowLayout *flowLayout = [[UICollectionViewFlowLayout alloc]init];
    flowLayout.minimumLineSpacing = 8.0f;
    flowLayout.minimumInteritemSpacing = 8.0f;
    flowLayout.sectionInset = UIEdgeInsetsMake(225*PIX+10, 15, 10, 15);
    flowLayout.scrollDirection = UICollectionViewScrollDirectionHorizontal;
    float height =0;
    NSInteger count = [_cateData allKeys].count/2;
    for (int i= 0; i<count; i++) {
        NSString *index = [NSString stringWithFormat:@"%d",i];
        CGSize size = CGSizeFromString([self.cateData objectForKey:index]);
        height = height+size.height;
        DLog(@" index = %@ height = %f",index,height);
    }
    _collection_height = height;
    float spaceHeight =8*(count-1)+22;
    _collectionView = [[UICollectionView alloc]initWithFrame:CGRectMake(0, 0*PIX, SCREEN_WIDTH, 225*PIX+height+spaceHeight) collectionViewLayout:flowLayout];
    _collectionView.delegate = self;
    _collectionView.dataSource = self;
    _collectionView.backgroundColor = [UIColor whiteColor];
    _collectionView.scrollEnabled = YES;
    [ _scrollView addSubview:_collectionView];
    [self addBanner];
    [_collectionView registerClass:[CateCollectionCell class] forCellWithReuseIdentifier:@"cateCell"];
    [_collectionView registerClass:[UICollectionReusableView class]
       forSupplementaryViewOfKind:UICollectionElementKindSectionHeader
              withReuseIdentifier:@"MainHeader"];
}

#pragma mark UICollection
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return _dataList.count;
}
- ( UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    CateModel *cate = self.dataList[indexPath.row];
    CateCollectionCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"cateCell" forIndexPath:indexPath];
     NSString *urlString = [cate.logo stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    [cell.logoView sd_setImageWithURL:[NSURL URLWithString:urlString]];
    return cell;
}
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    CateModel *cate = self.dataList[indexPath.row];
    SubCateViewController *sub = [SubCateViewController new];
    sub.subCate = cate;
    [self.navigationController pushViewController:sub animated:YES];
}
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    NSString *index = [NSString stringWithFormat:@"%ld",(long)indexPath.row];
    CGSize size = CGSizeFromString([self.cateData objectForKey:index]);
    return size;
}
/*
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout referenceSizeForHeaderInSection:(NSInteger)section{
    CGSize size = CGSizeMake(SCREEN_WIDTH, 225*PIX);
    return size;
}
- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath{
    //如果是头视图
    UICollectionReusableView *reusableView;
    if ([kind isEqualToString:UICollectionElementKindSectionHeader]) {
        reusableView=[collectionView dequeueReusableSupplementaryViewOfKind:kind withReuseIdentifier:@"MainHeader" forIndexPath:indexPath];
        CGRect rect = CGRectMake(0, 0, SCREEN_WIDTH, 225*PIX);
        NSMutableArray *imgUrls = [NSMutableArray arrayWithCapacity:0];
        for (Banner *b in self.bannerArray) {
            [imgUrls addObject:b.imgurl];
        }
        _cycleScrollView = [SDCycleScrollView cycleScrollViewWithFrame:rect imageNamesGroup:imgUrls];
        _cycleScrollView.pageControlStyle = SDCycleScrollViewPageContolStyleClassic;
        _cycleScrollView.pageDotColor = TABLE_COLOR;
        _cycleScrollView.delegate = self;
        _cycleScrollView.currentPageDotColor = THEME_COLOR;
        _cycleScrollView.showPageControl = YES;
        [ reusableView addSubview:_cycleScrollView];
    }
    return reusableView;
}
*/

-(void)loadTableView{
    NSInteger count = [_cateData allKeys].count/2;
    float spaceHeight =8*(count-1)+24;
    _tableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 225*PIX+_collection_height+spaceHeight, SCREEN_WIDTH, 950) style:UITableViewStylePlain];
    
    _scrollView.contentSize = CGSizeMake(SCREEN_WIDTH, 225*PIX+_collection_height+spaceHeight+950);
    _tableView.delegate = self;
    _tableView.dataSource = self;
    _tableView.scrollEnabled = NO;
    _tableView.backgroundColor =TABLE_COLOR;
    [_tableView  setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    [ _scrollView addSubview:_tableView];
    _tableView.tableFooterView = [[UIView alloc]init];
    [_tableView registerClass:[LunListTableViewCell class] forCellReuseIdentifier:@"newarticleCell"];
}

#pragma UITableViewDelegate&&DataSource

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return _dataArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    LunListTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"newarticleCell" forIndexPath:indexPath];
   cell.selectionStyle =UITableViewCellSelectionStyleNone;
    LunList *newarticle = _dataArray[indexPath.row];
    NSString *urlString = [newarticle.logo stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    [cell.logoView sd_setImageWithURL:[NSURL URLWithString:urlString]];
    cell.titleLabel.text = newarticle.name;
    return cell;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 180.0f;
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 50.0f;
}
- (nullable UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    UIView *header = [[UIView alloc]init];
    header.backgroundColor = [UIColor whiteColor];
    UILabel *label = [UILabel new];
    label.text = @"自在生活";
    label.textAlignment = NSTextAlignmentLeft;
    [header addSubview:label];
    [label mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(header).offset(20);
        make.centerY.equalTo(header);
        make.size.mas_equalTo(CGSizeMake(120, 30));
    }];
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(moreTap)];
    UILabel *moreLabel = [UILabel new];
    moreLabel.text = @"更多>>";
    moreLabel.font = [UIFont systemFontOfSize:15.0f];
    moreLabel.textAlignment = NSTextAlignmentRight;
    moreLabel.userInteractionEnabled = YES;
    [moreLabel addGestureRecognizer:tap];
    [header addSubview:moreLabel];
    [moreLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(header).offset(-20);
        make.centerY.equalTo(header);
        make.size.mas_equalTo(CGSizeMake(120, 30));
    }];
    return header;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    
     LunList *newarticle = _dataArray[indexPath.row];
    DetailViewController *detail = [DetailViewController new];
    detail.artid = newarticle.id;
    detail.artTitle = newarticle.name;
    DLog(@"id = %@,name = %@",newarticle.id,newarticle.name);
    [self.navigationController pushViewController:detail animated:YES];
    
}

-(void)moreTap {
    DLog(@"点击更多");
    LunListViewController *lun = [LunListViewController new];
    lun.path = articlePath;
    lun.navigationItem.title = @"自由生活";
    [self.navigationController pushViewController:lun animated:YES];
}

#pragma mark Location

-(void)locate {
    
    self.locationManager = [[AMapLocationManager alloc]init];
    // 带逆地理信息的一次定位（返回坐标和地址信息）
    [self.locationManager setDesiredAccuracy:kCLLocationAccuracyHundredMeters];
    //   定位超时时间，最低2s，此处设置为2s
    self.locationManager.locationTimeout =20;
    //   逆地理请求超时时间，最低2s，此处设置为2s
    self.locationManager.reGeocodeTimeout = 10;
    // 带逆地理（返回坐标和地址信息）。将下面代码中的 YES 改成 NO ，则不会返回地址信息。
    WS(weakSelf)
    [self.locationManager requestLocationWithReGeocode:NO completionBlock:^(CLLocation *location, AMapLocationReGeocode *regeocode, NSError *error) {
        if (error){
            DLog(@"locError:{%ld - %@};", (long)error.code, error.localizedDescription);
            if (error.code == AMapLocationErrorLocateFailed){
                DLog(@"定位禁止");
                AppDel.cityid = @"130123";
                [weakSelf hideHUD];
                [weakSelf getMainBannerWithCityid:AppDel.cityid];
            }
        }
        
        [weakSelf getCityidWithLocation:location];
    }];
}

-(void)getCityidWithLocation:(CLLocation *)location{
    WS(weakSelf)
    DLog(@"location=%@",location);
    [self showHUD];
    [self.manager getCityidWithLocationPath:locationPath location:location completion:^(id response) {
        if ([response[@"data"][@"cityid"] isKindOfClass:[NSArray class]]) {
            DLog(@"定位失败");
            AppDel.cityid = @"130123";
            [weakSelf hideHUD];
            [weakSelf getMainBannerWithCityid:AppDel.cityid];
        }else{
            NSString *cityid = response[@"data"][@"cityid"];
            AppDel.cityid = cityid;
            [weakSelf hideHUD];
            [weakSelf getMainBannerWithCityid:AppDel.cityid];
        }
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        
    }];
    
    
    
}

-(void)getMainBannerWithCityid:(NSString *)cityid{
    WS(weakSelf)
    _bannerArray = [NSMutableArray arrayWithCapacity:0];
    [self showHUD];
    [self.manager getMainBannerWithPath:bannerPath cityid:cityid completion:^(id response) {
        DLog(@"response = %@",response);
        [response[@"data"] enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
             [weakSelf hideHUD];
            Banner *b = [[Banner alloc]init];
            [b setValuesForKeysWithDictionary:obj];
            [weakSelf.bannerArray addObject:b];
        }];
         //[weakSelf addBanner];
        [weakSelf getCatesWithCityid:AppDel.cityid];
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
         [weakSelf hideHUD];
        WTOAST(@"网络连接错误");
    }];
}

-(void)getCatesWithCityid:(NSString *)cityid{
    WS(weakSelf)
    
    [self.manager getMainBannerWithPath:catesPath cityid:cityid completion:^(id response) {
        if ([response[@"code"] integerValue]==0) {
            weakSelf.dataList = [NSMutableArray arrayWithCapacity:0];
            weakSelf.cateData = [NSMutableDictionary dictionaryWithCapacity:0];
            for (id obj in response[@"data"]) {
                [weakSelf hideHUD];
                CateModel *cate = [[CateModel alloc]init];
                [cate setValuesForKeysWithDictionary:obj];
                [weakSelf.dataList addObject:cate];
            }
        }
        [weakSelf getSizeList];
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
         [weakSelf hideHUD];
         WTOAST(@"网络连接错误");
    }];
}
-(void)getSizeList{
    WS(weakSelf)
    for (int i = 0; i<_dataList.count; i++) {
        UIImageView *imgView = [[UIImageView alloc]init];
        CateModel *cate = self.dataList[i];
        NSString *urlString = [cate.logo stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        [imgView sd_setImageWithURL:[NSURL URLWithString:urlString] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
            NSString *index = [NSString stringWithFormat:@"%d",i];
            CGSize size = image.size;
            float width = (SCREEN_WIDTH-40)/2.0f;
            float height = size.height*(width/size.width);
            CGSize newSize = CGSizeMake(width, height);
            NSString *sizeString = NSStringFromCGSize(newSize);
            [weakSelf.cateData setObject:sizeString forKey:index];
            NSArray *keys = [weakSelf.cateData allKeys];
            if (keys.count>=weakSelf.dataList.count) {
                 [self loadCollection];//加载瀑布流
                [self getNewArticle];
            }
        }];
        [self.view addSubview:imgView];
    }
}

//newArticle

-(void)getNewArticle{
    WS(weakSelf)
    [self showHUD];
    _dataArray  = [NSMutableArray arrayWithCapacity:0];
    [self.manager getNewArticleWithPath:newarticlePath cityid:AppDel.cityid completion:^(id response) {
        [weakSelf hideHUD];
        if ([response[@"code"] integerValue]==0) {
            [response[@"data"] enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                LunList *newarticle = [[LunList alloc]init];
                [newarticle setValuesForKeysWithDictionary:obj];
                [weakSelf.dataArray addObject:newarticle];
            }];
           [weakSelf loadTableView];
        }else{
            WTOAST(response[@"msg"]);
        }
        
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        WTOAST(@"请求失败");
        DLog(@"error = %@",error.description);
        [weakSelf hideHUD];
    }];
    
    
}





-(HTTPManager *)manager{
    if (!_manager) {
        _manager = [HTTPManager httpManager];
    }
    return _manager;
}

-(void)viewWillAppear:(BOOL)animated{
    self.navigationItem.title = @"AR黄页";
    self.tabBarController.tabBar.hidden = NO;
}
-(void)viewWillDisappear:(BOOL)animated{
    self.navigationItem.title = @"";
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
