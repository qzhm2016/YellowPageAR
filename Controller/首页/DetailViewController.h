//
//  DetailViewController.h
//  ARYellowPage
//
//  Created by youdian on 2018/4/20.
//  Copyright © 2018年 YouDianAd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <WebKit/WebKit.h>

@interface DetailViewController : UIViewController<WKNavigationDelegate>

@property (nonatomic ,copy) NSString *artid;
@property (nonatomic ,copy) NSString *artTitle;
@property(nonatomic,copy)NSString *urlStr;
@property (strong, nonatomic)Article *detailArticle;
@property (nonatomic ,copy) NSString *status;
@property (strong, nonatomic)UIButton *mainButton;
@property (strong, nonatomic)UIButton *shareButton;
@property (strong, nonatomic)UIButton *PlayButton;
@property (strong, nonatomic)UIButton *collectButton;
@property (nonatomic ,copy) NSString *urlPath;


@end
