//
//  SearchResultVC.m
//  ARYellowPage
//
//  Created by youdian on 2018/4/23.
//  Copyright © 2018年 YouDianAd. All rights reserved.
//

#import "SearchResultVC.h"
#import "UIImageView+WebCache.h"
#import "DetailViewController.h"

@interface SearchResultVC ()<UICollectionViewDelegate,UICollectionViewDataSource,UITextFieldDelegate>

@end

@implementation SearchResultVC


-(void)viewWillAppear:(BOOL)animated{
    self.navigationItem.title = self.key;
    self.tabBarController.tabBar.hidden = YES;
}
-(void)viewWillDisappear:(BOOL)animated{
    self.navigationItem.title = @"";
}
- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = TABLE_COLOR;
    if (self.key.length>0) {
       
         [self getarticle];
    }else{
        [self requestSubCates];
    }
   
    [self searchHeader];
    [self loadSearchCollectionView];
    
    // Do any additional setup after loading the view.
}
-(void)loadSearchCollectionView{
    UICollectionViewFlowLayout *flowLayout = [[UICollectionViewFlowLayout alloc]init];
    flowLayout.minimumLineSpacing = 10.0f;
    flowLayout.minimumInteritemSpacing = 10.0f;
    flowLayout.sectionInset = UIEdgeInsetsMake(15, 10, 10, 10);
//    CGFloat itemWidth = (SCREEN_WIDTH-30)/2;
//    float itemHeight = self.key.length>0?itemWidth-45:itemWidth+55;
//    flowLayout.itemSize = CGSizeMake(itemWidth, itemHeight);
    flowLayout.scrollDirection = UICollectionViewScrollDirectionVertical;
    _collectionView = [[UICollectionView alloc]initWithFrame:CGRectMake(0, 64+65, SCREEN_WIDTH, SCREEN_HEIGHT-64) collectionViewLayout:flowLayout];
    _collectionView.delegate = self;
    _collectionView.dataSource = self;
  
    _collectionView.backgroundColor = TABLE_COLOR;
    _collectionView.scrollEnabled = YES;
    [self.view addSubview:_collectionView];
    [_collectionView registerClass:[NavCollectionViewCell class] forCellWithReuseIdentifier:@"searchCell"];
    
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return _dataList.count;
}
- ( UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
     LunList *search = self.dataList[indexPath.row];
    NavCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"searchCell" forIndexPath:indexPath];

    DLog(@"logo=%@",search.logo);
    NSString *urlString = [search.logo stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    [cell.logoView sd_setImageWithURL:[NSURL URLWithString:urlString]];
    cell.titleLabel.text = search.name;
    return cell;
}
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    CGFloat itemWidth = (SCREEN_WIDTH-30)/2;
    float itemHeight = self.key.length>0?itemWidth-45:itemWidth+55;
    return CGSizeMake(itemWidth, itemHeight);
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    
    [self.view endEditing:YES];
    LunList *search = self.dataList[indexPath.row];
    DetailViewController *detail = [DetailViewController new];
    detail.artid = search.id;
    detail.artTitle = search.name;
    [self.navigationController pushViewController:detail animated:YES];
}



-(void)searchHeader{
    UIView *header = [[UIView alloc]init];
    header.frame = CGRectMake(0, 64, SCREEN_WIDTH, 65);
    header.backgroundColor = THEME_COLOR;
    [self.view addSubview:header];
    UITextField *textField = [[UITextField alloc]init];
    textField.borderStyle = UITextBorderStyleRoundedRect;
    textField.placeholder = @"搜索内容";
    textField.layer.cornerRadius = 20.0f;
    textField.clipsToBounds = YES;
    textField.clearButtonMode = UITextFieldViewModeAlways;
    textField.leftView = [[UIImageView alloc]initWithImage:IMAGE_NAME(@"icon_search_gray")];
    textField.leftViewMode =UITextFieldViewModeAlways;
    textField.delegate = self;
    [textField addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
    [header addSubview:textField];
    [textField mas_makeConstraints:^(MASConstraintMaker *make) {
        make.center.equalTo(header);
        make.size.mas_equalTo(CGSizeMake(SCREEN_WIDTH-40, 40));
    }];
}



#pragma mark UITextField Delegate

- (void)textFieldDidChange:(UITextField *)textField {
    
}
- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    textField.returnKeyType = UIReturnKeySearch;
    return YES;
}
- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    if (textField.text.length<1) {
        WTOAST(@"请输入搜索内容");
    }else{
        self.key = textField.text;
        [self getarticle];
    }
    
    return YES;
    
}
-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event{
    [self.view endEditing:YES];
}
-(void)getarticle{
    WS(weakSelf)
    _dataList = [NSMutableArray arrayWithCapacity:0];
    [self showHUD];
    [self.manager getSerachReslutsWithPath:getarticlePath cityid:AppDel.cityid searchKey:self.key completion:^(id response) {
        DLog(@"Res = %@",response);
        [weakSelf hideHUD];
        if ([response[@"code"] integerValue]==0) {
            [response[@"data"] enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                LunList *search = [[LunList alloc]init];
                [search setValuesForKeysWithDictionary:obj];
                [weakSelf.dataList addObject:search];
            }];
        }else{
             WTOAST(response[@"msg"]);
        }
          [weakSelf.collectionView reloadData];
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        WTOAST(@"网络连接错误");
           [weakSelf hideHUD];
    }];
}

-(void)requestSubCates{
    WS(weakSelf)
    [self showHUD];
    _dataList = [NSMutableArray arrayWithCapacity:0];
    [self.manager getSubCatesWithPath:getsubcatesPath cateid:self.artcleid completion:^(id response) {
        [weakSelf hideHUD];
        if ([response[@"code"] integerValue]==0) {
            [response[@"data"] enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                CateModel *subCate = [[CateModel alloc]init];
                [subCate setValuesForKeysWithDictionary:obj];
                [weakSelf.dataList addObject:subCate];
            }];
            [weakSelf.collectionView reloadData];
        }else{
            WTOAST(response[@"msg"]);
        }
        
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        [weakSelf hideHUD];
        WTOAST(@"请求失败");
    }];
}


-(HTTPManager *)manager{
    if (!_manager) {
        _manager = [HTTPManager httpManager];
    }
    return _manager;
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
