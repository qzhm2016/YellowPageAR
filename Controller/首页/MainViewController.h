//
//  MainViewController.h
//  ARYellowPage
//
//  Created by youdian on 2018/4/16.
//  Copyright © 2018年 YouDianAd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AMapLocationKit/AMapLocationKit.h>


@interface MainViewController : UIViewController<CLLocationManagerDelegate>


@property (strong, nonatomic)HTTPManager *manager;
@property (strong, nonatomic)AMapLocationManager *locationManager;
@property (strong, nonatomic)NSMutableArray *bannerArray;
@end
