//
//  SearchResultVC.h
//  ARYellowPage
//
//  Created by youdian on 2018/4/23.
//  Copyright © 2018年 YouDianAd. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SearchResultVC : UIViewController

@property (nonatomic ,copy) NSString *key;
@property (nonatomic ,copy) NSString *artcleid;
@property (strong, nonatomic)HTTPManager *manager;
@property (strong, nonatomic)UICollectionView *collectionView;
@property (strong, nonatomic)NSMutableArray *dataList;
@end
