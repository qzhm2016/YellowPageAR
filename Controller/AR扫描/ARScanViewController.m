//
//  ARScanViewController.m
//  ARYellowPage
//
//  Created by youdian on 2018/4/28.
//  Copyright © 2018年 YouDianAd. All rights reserved.
//

#import "ARScanViewController.h"
#import "ViewController.h"
#import "UIImageView+WebCache.h"
#import "MJRefresh.h"
#import "DetailViewController.h"
#import "SearchResultVC.h"

@interface ARScanViewController ()<UITableViewDataSource,UITableViewDelegate,UITextFieldDelegate>

@end

@implementation ARScanViewController


- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.title=@"AR扫描";
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc]initWithImage:IMAGE_NAME(@"返回") style:UIBarButtonItemStylePlain target:self action:@selector(back)];
    self.view.backgroundColor = [UIColor whiteColor];
    _indexPage = 1;
    _dataList = [NSMutableArray arrayWithCapacity:0];
    [self loadTableView];
    [self requestApparticles];
    
    // Do any additional setup after loading the view.
}

-(void)back {
    self.tabBarController.selectedIndex = AppDel.selectIndex;
}
-(void)viewWillAppear:(BOOL)animated{
    self.tabBarController.tabBar.hidden = YES;
}
-(void)viewWillDisappear:(BOOL)animated{
     self.tabBarController.tabBar.hidden = NO;
}

-(void)loadTableView{
    
    _tableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 64, SCREEN_WIDTH, SCREEN_HEIGHT-64) style:UITableViewStylePlain];
    _tableView.delegate = self;
    _tableView.dataSource = self;
    _tableView.backgroundColor =TABLE_COLOR;
    _tableView.tableHeaderView = [self tableHeaderView];
    [self.view addSubview:_tableView];
    _tableView.tableFooterView = [[UIView alloc]init];
    [_tableView registerClass:[LunListTableViewCell class] forCellReuseIdentifier:@"ApparticleCell"];
    
    WS(weakSelf)
    MJRefreshNormalHeader *header = [MJRefreshNormalHeader  headerWithRefreshingBlock:^{
        weakSelf.indexPage = 1;
        [weakSelf requestApparticles];
    }];
    _tableView.mj_header = header;
    header.lastUpdatedTimeLabel.hidden = NO;
    MJRefreshBackNormalFooter  *footer =[MJRefreshBackNormalFooter footerWithRefreshingBlock:^{
        weakSelf.indexPage++;
        [weakSelf requestApparticles];
        
    }];
    _tableView.mj_footer = footer;
    
    if (@available(iOS 11.0, *)) {
        self.tableView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
    } else {
        self.automaticallyAdjustsScrollViewInsets=NO;
    }
}
-(void)scanAR{
    [self.navigationController pushViewController:[ViewController new] animated:YES];
}

#pragma UITableViewDelegate&&DataSource

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return _dataList.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    LunListTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"ApparticleCell" forIndexPath:indexPath];
    LunList *app = self.dataList[indexPath.row];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    NSString *urlString = [app.logo stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    [cell.logoView sd_setImageWithURL:[NSURL URLWithString:urlString]];
    cell.titleLabel.text = app.name;
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    LunList *app = self.dataList[indexPath.row];
    DetailViewController *detail = [DetailViewController new];
    detail.artid = app.id;
    detail.urlPath = @"/api/article/appdetail";
    detail.artTitle = app.name;
    detail.status = @"3";
    [self.navigationController pushViewController:detail animated:YES];
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 165.0f*PIX;
}

-(UIView *)tableHeaderView{
    UIView *header = [UIView new];
    header.bounds = CGRectMake(0, 0, SCREEN_WIDTH, 165*PIX+65);
    header.backgroundColor =THEME_COLOR;
    UIImageView *imgView = [UIImageView new];
    imgView.image = IMAGE_NAME(@"ar_tui_top");
    imgView.frame= CGRectMake(0, 0, SCREEN_WIDTH, 165*PIX);
    [header addSubview:imgView];
    imgView.userInteractionEnabled = YES;
    UITapGestureRecognizer *tap  = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(tap)];
    [imgView addGestureRecognizer:tap];
    
    UITextField *textField = [[UITextField alloc]init];
    textField.frame = CGRectMake(20, 165*PIX+13, SCREEN_WIDTH-40, 40);
    textField.borderStyle = UITextBorderStyleRoundedRect;
    textField.placeholder = @"搜索内容";
    textField.layer.cornerRadius = 20.0f;
    textField.clipsToBounds = YES;
    textField.clearButtonMode = UITextFieldViewModeAlways;
    textField.leftView = [[UIImageView alloc]initWithImage:IMAGE_NAME(@"icon_search_gray")];
    textField.leftViewMode =UITextFieldViewModeAlways;
    textField.contentMode= UIViewContentModeCenter;
    textField.leftView.frame = CGRectMake(0, 0, 20, 20);
    textField.delegate = self;
    [textField addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
    [header addSubview:textField];
    return header;
}


-(void)tap {
    [self scanAR];
}
-(void)requestApparticles{
    WS(weakSelf)
    HTTPManager *manager = [HTTPManager httpManager];
    NSString *page = [NSString stringWithFormat:@"%ld",_indexPage];
    [manager getArticleApparticlesWithPath:@"/api/article/apparticles" page:page completion:^(id response) {
        [weakSelf.tableView.mj_header endRefreshing];
        [weakSelf.tableView.mj_footer endRefreshing];
        if ([response[@"code"] integerValue]==0) {
            [weakSelf.dataList removeAllObjects];
            [response[@"data"] enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                LunList *lun = [[LunList alloc]init];
                [lun setValuesForKeysWithDictionary:obj];
                [weakSelf.dataList addObject:lun];
            }];
            [weakSelf.tableView reloadData];
        }else{
            WTOAST(response[@"msg"]);
        }
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        [weakSelf.tableView.mj_header endRefreshing];
        [weakSelf.tableView.mj_footer endRefreshing];
        WTOAST(@"网络连接错误");
    }];
}



#pragma mark UITextField Delegate

- (void)textFieldDidChange:(UITextField *)textField {
    
}
- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    textField.returnKeyType = UIReturnKeySearch;
    return YES;
}
- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    if (textField.text.length<1) {
        WTOAST(@"请输入搜索内容");
    }else{
        SearchResultVC *result = [SearchResultVC new];
        result.key = textField.text;
        [self.navigationController pushViewController:result animated:YES];
    }
    
    return YES;
    
}
-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event{
    [self.view endEditing:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
