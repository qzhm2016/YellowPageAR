//
//  ARScanViewController.h
//  ARYellowPage
//
//  Created by youdian on 2018/4/28.
//  Copyright © 2018年 YouDianAd. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ARScanViewController : UIViewController

@property (strong, nonatomic)UITableView *tableView;
@property (strong, nonatomic)NSMutableArray *dataList;
@property (assign, nonatomic)NSInteger indexPage;
@end
