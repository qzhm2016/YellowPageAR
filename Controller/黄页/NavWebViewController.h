//
//  NavWebViewController.h
//  ARYellowPage
//
//  Created by youdian on 2018/5/7.
//  Copyright © 2018年 YouDianAd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <WebKit/WebKit.h>

@interface NavWebViewController : UIViewController<WKNavigationDelegate>

@property(nonatomic,copy)NSString *urlStr;
@end
