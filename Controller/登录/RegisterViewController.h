//
//  RegisterViewController.h
//  ARYellowPage
//
//  Created by youdian on 2018/4/16.
//  Copyright © 2018年 YouDianAd. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RegisterViewController : UIViewController

@property (strong, nonatomic)UITextField *phoneTF;//手机号
@property (strong, nonatomic)UITextField *imgCodeTF;//图片验证码
@property (strong, nonatomic)UITextField *pwdTF;//密码
@property (strong, nonatomic)UITextField *rePwdTF;//确认密码
@property (strong, nonatomic)UITextField *reCodeTF;//验证码
@property (strong, nonatomic)UIButton *imgButton;
@property (strong, nonatomic)UIButton *codeButton;
  
@end
