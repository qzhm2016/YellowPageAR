//
//  LoginViewController.m
//  ARYellowPage
//
//  Created by youdian on 2018/4/16.
//  Copyright © 2018年 YouDianAd. All rights reserved.
//

#import "LoginViewController.h"




@interface LoginViewController ()<UITextFieldDelegate>

@property (strong, nonatomic)UITextField *nameTF;
@property (strong, nonatomic)UITextField *pwdTF;
@end

@implementation LoginViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor =[UIColor whiteColor];
    [self loginUI];
    
    // Do any additional setup after loading the view.
}


-(void)viewWillAppear:(BOOL)animated{
    self.navigationController.navigationBar.hidden = YES;
    self.tabBarController.tabBar.hidden = YES;
}

-(void)loginUI{
    WS(weakSelf)
    UIButton *back = [UIButton buttonWithType:UIButtonTypeCustom];
    [back setImage:IMAGE_NAME(@"back") forState:UIControlStateNormal];
    [back addTarget:self action:@selector(back) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:back];
    [back mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(weakSelf.view).offset(30);
        make.left.equalTo(weakSelf.view).offset(10);
        make.size.mas_equalTo(CGSizeMake(25, 25));
    }];
    
    
    
    UIImageView *logo = [[UIImageView alloc]initWithImage:IMAGE_NAME(@"activity_login_logo_iv")];
    [self.view addSubview:logo];
    [logo mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(weakSelf.view).offset(80);
        make.centerX.equalTo(weakSelf.view);
        make.size.mas_equalTo(CGSizeMake(120, 120));
    }];
    UIImageView *bottom = [[UIImageView alloc]initWithImage:IMAGE_NAME(@"activity_login_bottom_iv")];
    [self.view addSubview:bottom];
    [bottom mas_makeConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(CGSizeMake(SCREEN_WIDTH, 142*PIX));
        make.centerX.equalTo(weakSelf.view);
        make.bottom.equalTo(weakSelf.view);
    }];
    
    UILabel *logoLabel = [UILabel new];
    logoLabel.text = @"AR黄页";
    logoLabel.textAlignment = NSTextAlignmentCenter;
    logoLabel.font = [UIFont systemFontOfSize:25.0f];
    [self.view addSubview:logoLabel];
    [logoLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(logo.mas_bottom).offset(5);
        make.centerX.equalTo(logo);
        make.size.mas_equalTo(CGSizeMake(150, 50));
    }];
    
    NSArray *array = @[@"user_name_iv",@"user_pass_iv"];
    for (int i = 0; i<array.count; i++) {
        UIImageView *imgView = [[UIImageView alloc]initWithImage:IMAGE_NAME(array[i])];
        [self.view addSubview:imgView];
        [imgView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.size.mas_equalTo(CGSizeMake(63/2.0f, 63/2.0f));
            make.left.equalTo(weakSelf.view).offset(45);
            make.top.equalTo(logoLabel.mas_bottom).offset(30+i*60);
        }];
        UITextField *textField = [[UITextField alloc]init];
        textField.borderStyle = UITextBorderStyleNone;
        textField.placeholder = @[@"手机号",@"密码"][i];
        textField.tag = 20+i;
        textField.textColor = THEME_COLOR;
        textField.clearButtonMode = UITextFieldViewModeWhileEditing;
        textField.delegate = self;
        [textField addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
        [self.view addSubview:textField];
        [textField mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.height.equalTo(imgView);
            make.left.equalTo(imgView.mas_right).offset(5);
            make.right.equalTo(weakSelf.view).offset(-45);
        }];
        
        UIImageView *line = [[UIImageView alloc]init];
        line.backgroundColor =THEME_COLOR;
        [self.view addSubview:line];
        [line mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(textField.mas_bottom);
            make.left.right.equalTo(textField);
            make.height.mas_equalTo(1.5);
        }];
    }
    _nameTF = (UITextField *)[self.view viewWithTag:20];
    _pwdTF = (UITextField *)[self.view viewWithTag:21];
    _pwdTF.secureTextEntry = YES;
    
   UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    [button setTitle:@"登录"forState:UIControlStateNormal];
    [button setBackgroundColor:THEME_COLOR];
    button.titleLabel.font = [UIFont systemFontOfSize:18.0f];
    button.layer.cornerRadius = 25.0f;
    button.tag = 10;
    [button setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [button addTarget:self action:@selector(btnClick:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:button];
    [button mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(weakSelf.pwdTF.mas_bottom).offset(50*PIX);
        make.centerX.equalTo(weakSelf.view);
        make.size.mas_equalTo(CGSizeMake(SCREEN_WIDTH-85, 50));
    }];
    UIButton *registerBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [registerBtn setTitle:@"注册"forState:UIControlStateNormal];
    [registerBtn setBackgroundColor:[UIColor clearColor]];
    registerBtn.tag = 11;
    registerBtn.titleLabel.font = [UIFont systemFontOfSize:18.0f];
    [ registerBtn setTitleColor:THEME_COLOR forState:UIControlStateNormal];
    [ registerBtn addTarget:self action:@selector(btnClick:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview: registerBtn];
    [ registerBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(button.mas_bottom);
        make.left.equalTo(button);
        make.size.mas_equalTo(CGSizeMake(50, 50));
    }];
    UIButton *forgetBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [forgetBtn setTitle:@"忘记密码？"forState:UIControlStateNormal];
    [forgetBtn setBackgroundColor:[UIColor clearColor]];
    forgetBtn.titleLabel.font = [UIFont systemFontOfSize:18.0f];
    [forgetBtn setTitleColor:THEME_COLOR forState:UIControlStateNormal];
    [ forgetBtn addTarget:self action:@selector(btnClick:) forControlEvents:UIControlEventTouchUpInside];
    forgetBtn.tag = 12;
    [self.view addSubview: forgetBtn];
    [ forgetBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(button.mas_bottom);
        make.right.equalTo(button);
        make.size.mas_equalTo(CGSizeMake(100, 50));
    }];
    
    
    
    
    
    
    
}

-(void)back {
    [self dismissViewControllerAnimated:YES completion:nil];
}
-(void)btnClick:(UIButton*)button{
    switch (button.tag) {
        case 10:
            [self login];
            break;
        case 11:
          [self goViewControllerWithName:@"RegisterViewController"];
            break;
        case 12:
         [self goViewControllerWithName:@"ChangePwdViewController"];
            break;
        default:
            break;
    }
    
}

-(void)login{
    if (![NSString isMobile:_nameTF.text]) {
        CWTOAST(@"请输入正确的手机号");
        return;
    }
    if (_pwdTF.text.length<1) {
        CWTOAST(@"请输入密码");
        return;
    }
    
    [self.manager loginWithPath:loginPath userPhone:_nameTF.text password:_pwdTF.text completion:^(id response) {
        if ([response[@"code"] integerValue]==0) {
            [USER_DEFAULT setObject:response[@"data"] forKey:@"token"];
            [USER_DEFAULT synchronize];
            [AppDel setRootWindowControllerTabBar];
        }
        WTOAST(response[@"msg"]);
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        WTOAST(@"网络连接错误");
    }];
}
- (void)textFieldDidChange:(UITextField *)textField {
    //限制手机号输入长度 大陆手机号 +86省去
    if (textField.tag==20) {
        if (textField.text.length > 11) {
            textField.text = [textField.text substringToIndex:11];
        }
    }
}
- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    if (textField ==_nameTF) {
        textField.keyboardType = UIKeyboardTypeNumberPad;
    }
    
    return YES;
}
-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [self.view endEditing:YES];
    
}

-(HTTPManager *)manager{
    if (!_manager) {
        _manager = [HTTPManager httpManager];
    }
    return _manager;
}
-(void)goViewControllerWithName:(NSString *)viewName{
    UIViewController *vc = [NSClassFromString(viewName) new];
    [self.navigationController pushViewController:vc animated:YES];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
