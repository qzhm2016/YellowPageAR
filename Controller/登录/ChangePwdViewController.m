//
//  ChangePwdViewController.m
//  ARYellowPage
//
//  Created by youdian on 2018/4/16.
//  Copyright © 2018年 YouDianAd. All rights reserved.
//

#import "ChangePwdViewController.h"

@interface ChangePwdViewController ()<UITextFieldDelegate>
@property (nonatomic, strong)  NSURLSessionDownloadTask *downloadTask;
@end

@implementation ChangePwdViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = TABLE_COLOR;
    self.navigationController.title = @"'忘记密码";
    [self forgetPwdUI];
    
    // Do any additional setup after loading the view.
}

-(void)forgetPwdUI{
    WS(weakSelf)
    UIImageView *imgView=  [[UIImageView alloc]initWithImage:IMAGE_NAME(@"forget_pass_top_iv")];
    [self.view addSubview:imgView];
    [imgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(weakSelf.view);
        make.centerX.equalTo(weakSelf.view);
        make.size.mas_equalTo(CGSizeMake(SCREEN_WIDTH, 180));
    }];
    UILabel *title = [UILabel new];
    title.text  = @"忘记密码";
    title.font = [UIFont systemFontOfSize:18.0f];
    title.textAlignment = NSTextAlignmentCenter;
    title.textColor = [UIColor whiteColor];
    [self.view addSubview:title];
    [title mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(weakSelf.view);
        make.top.equalTo(weakSelf.view).offset(20);
        make.size.mas_equalTo(CGSizeMake(120, 30));
    }];
    UIButton *back = [UIButton buttonWithType:UIButtonTypeCustom];
    [back setImage:IMAGE_NAME(@"返回") forState:UIControlStateNormal];
    [back addTarget:self action:@selector(back) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:back];
    [back mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(weakSelf.view).offset(30);
        make.left.equalTo(weakSelf.view).offset(10);
        make.size.mas_equalTo(CGSizeMake(25, 25));
    }];
    for (int i = 0; i<5; i++) {
        NSString *name = i>0?@"user_pass_iv":@"user_name_iv";
        UIImageView *nameView = [[UIImageView alloc]initWithImage:IMAGE_NAME(name)];
        [self.view addSubview:nameView];
        [nameView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(imgView.mas_bottom).offset(30+i*65);
            make.left.equalTo(weakSelf.view).offset(35);
            make.size.mas_equalTo(CGSizeMake(63/2.0f, 63/2.0f));
        }];
        UILabel *line = [UILabel new];
        line.backgroundColor = THEME_COLOR;
        [self.view addSubview:line];
        float width  = (i==1||i==4)?SCREEN_WIDTH-200:SCREEN_WIDTH-80;
        [line mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(nameView.mas_right);
            make.bottom.mas_equalTo(nameView).offset(2);
            make.size.mas_equalTo(CGSizeMake(width,1.5f));
        }];
        UITextField *textField = [[UITextField alloc]init];
        textField.borderStyle = UITextBorderStyleNone;
        textField.placeholder = @[@"手机号",@"图片验证码",@"请输入原密码",@"请输入新密码",@"验证码"][i];
        textField.tag = 20+i;
        textField.textColor = THEME_COLOR;
        textField.clearButtonMode = UITextFieldViewModeWhileEditing;
        textField.delegate = self;
        [textField addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
        [self.view addSubview:textField];
        float space = (i==1||i==4)?-135:35;
        [textField mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.height.equalTo(nameView);
            make.left.equalTo(nameView.mas_right).offset(5);
            make.right.equalTo(weakSelf.view).offset(space);
        }];
    }
    _phoneTF = (UITextField *)[self.view viewWithTag:20];
    _imgCodeTF = (UITextField *)[self.view viewWithTag:21];
    _pwdTF = (UITextField *)[self.view viewWithTag:22];
    _pwdTF.secureTextEntry = YES;
    _rePwdTF = (UITextField *)[self.view viewWithTag:23];
    _rePwdTF.secureTextEntry=YES;
    _reCodeTF = (UITextField *)[self.view viewWithTag:24];
    
    _imgButton =[UIButton buttonWithType:UIButtonTypeCustom];
    [_imgButton setTitle:@"图片验证码"forState:UIControlStateNormal];
    [_imgButton setTitleColor:THEME_COLOR forState:UIControlStateNormal];
    _imgButton.titleLabel.font = [UIFont systemFontOfSize:18.0f];
    _imgButton.layer.cornerRadius = 5.0f;
    _imgButton.tag = 10;
    _imgButton.layer.borderWidth = 1.5f;
    _imgButton.layer.borderColor = THEME_COLOR.CGColor;
    [_imgButton addTarget:self action:@selector(btnClick:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:_imgButton];
    [_imgButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(weakSelf.imgCodeTF).offset(3);
        make.left.equalTo(weakSelf.imgCodeTF.mas_right).offset(3);
        make.right.equalTo(weakSelf.view).offset(-15);
        make.height.mas_equalTo(45);
    }];
    _codeButton =[UIButton buttonWithType:UIButtonTypeCustom];
    [_codeButton setTitle:@"获取验证码"forState:UIControlStateNormal];
    [_codeButton setTitleColor:THEME_COLOR forState:UIControlStateNormal];
    _codeButton.titleLabel.font = [UIFont systemFontOfSize:18.0f];
    _codeButton.layer.cornerRadius = 5.0f;
    _codeButton.tag = 11;
    _codeButton.layer.borderWidth = 1.5f;
    _codeButton.layer.borderColor = THEME_COLOR.CGColor;
    [_codeButton addTarget:self action:@selector(btnClick:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:_codeButton];
    [_codeButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(weakSelf.reCodeTF).offset(3);
        make.left.equalTo(weakSelf.reCodeTF.mas_right).offset(3);
        make.right.equalTo(weakSelf.view).offset(-15);
        make.height.mas_equalTo(45);
    }];
    
    UIButton *registerButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [registerButton setTitle:@"修改密码"forState:UIControlStateNormal];
    [registerButton setBackgroundColor:THEME_COLOR];
    registerButton.titleLabel.font = [UIFont systemFontOfSize:18.0f];
    registerButton.layer.cornerRadius = 25.0f;
    registerButton.tag = 12;
    [registerButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [registerButton addTarget:self action:@selector(btnClick:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:registerButton];
    [registerButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(weakSelf.reCodeTF.mas_bottom).offset(40*PIX);
        make.centerX.equalTo(weakSelf.view);
        make.size.mas_equalTo(CGSizeMake(SCREEN_WIDTH-85, 50));
    }];
}

-(void)back{
    [self.navigationController popViewControllerAnimated:YES];
}
-(void)btnClick:(UIButton *)button{
    if (button.tag==10 ) {
        if (![NSString isMobile:_phoneTF.text]) {
            WTOAST(@"请输入正确格式手机号");
            return;
        }
        [self getimageViewNumberCode];
    }if (button.tag==11) {
        if (![NSString isMobile:_phoneTF.text]) {
            WTOAST(@"请输入正确格式手机号");
            return;
        }
        if (_imgCodeTF.text.length<1) {
            WTOAST(@"请输入图片验证码");
            return;
        }
        [self requestCode];
    }
    if (button.tag ==12) {
        if (![NSString isMobile:_phoneTF.text]) {
            WTOAST(@"请输入正确格式手机号");
            return;
        }
        if (_pwdTF.text.length<1) {
            WTOAST(@"请输入原密码");
            return;
        }
        if (_rePwdTF.text.length<1) {
            WTOAST(@"请输入新密码");
            return;
        }
        if (_reCodeTF.text.length<1) {
            WTOAST(@"请输入短信验证码");
            return;
        }
        [self requestChange];
    }
}

-(void)requestChange {
    HTTPManager *manager = [HTTPManager httpManager];
    [manager userChangePasswordWithPath:changePath userPhone:_phoneTF.text old_pwd:_pwdTF.text newPwd:_rePwdTF.text code:_reCodeTF.text completion:^(id response) {
        if ([response[@"code"] integerValue]==0) {
            [USER_DEFAULT setObject:response[@"data"] forKey:@"token"];
            [USER_DEFAULT synchronize];
            [AppDel setRootWindowControllerTabBar];
        }
        WTOAST(response[@"msg"]);
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        WTOAST(@"网络连接错误");
    }];
    
    
    
}
-(void)requestCode{
    WS(weakSelf)
    [self showHUDWithMessage:@"正在发送"];
    HTTPManager *manager = [HTTPManager httpManager];
    [manager getSmsCode:smsPath userPhone:_phoneTF.text imgNumber:_imgCodeTF.text type:@"2" completion:^(id res) {
        [weakSelf hideHUD];
        WTOAST(res[@"msg"]);
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        [weakSelf hideHUD];
        WTOAST(@"网络连接错误");
    }];
    __block NSInteger timeout = 60;
    dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
   dispatch_source_t timer = dispatch_source_create(DISPATCH_SOURCE_TYPE_TIMER, 0, 0, queue);
    dispatch_source_set_timer(timer, DISPATCH_TIME_NOW, 1.0 * NSEC_PER_SEC, 0 * NSEC_PER_SEC);
    dispatch_source_set_event_handler(timer, ^{
        if (timeout <=0) {
            dispatch_source_cancel(timer);
            dispatch_async(dispatch_get_main_queue(), ^{
                [weakSelf.codeButton setTitle:@"发送验证码" forState:UIControlStateNormal];
                weakSelf.codeButton.userInteractionEnabled = YES;
            });
        }else{
            NSInteger sec = timeout%61;
            NSString *strTime = [NSString stringWithFormat:@"%.2ld",sec];
            dispatch_async(dispatch_get_main_queue(), ^{
                [weakSelf.codeButton setTitle:[NSString stringWithFormat:@"%@秒",strTime] forState:UIControlStateNormal];
                weakSelf.codeButton.userInteractionEnabled = NO;
            });
            timeout--;
        }
    });
    dispatch_resume(timer);
    
}
-(void)getimageViewNumberCode{
    WS(weakSelf)
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSString *cachesPath = [NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES) lastObject];
    NSString *path = [cachesPath stringByAppendingPathComponent:@"regpic.html"];
    BOOL res = [fileManager removeItemAtPath:path error:nil];
    if (res) {
        DLog(@"文件删除成功");
    }
    else{
        DLog(@"文件是否存在: %@",[fileManager isExecutableFileAtPath:path]?@"YES":@"NO");
    }
    
    NSString *actionStamp = [NSString stringWithFormat:@"%ld",(long)[[NSDate date] timeIntervalSince1970]];
    NSString *urlStr =[NSString stringWithFormat:@"%@/api/sms/regpic?phone=%@&action=%@",HOST,_phoneTF.text,actionStamp];
    NSURL *url = [NSURL URLWithString:urlStr];
    //默认配置
    NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
    //AFN3.0+基于封住URLSession的句柄
    AFURLSessionManager *manager = [[AFURLSessionManager alloc] initWithSessionConfiguration:configuration];
    //请求
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    _downloadTask  = [manager downloadTaskWithRequest:request progress:^(NSProgress * _Nonnull downloadProgress) {
        // 回到主队列刷新UI
        dispatch_async(dispatch_get_main_queue(), ^{
            // 设置进度条的百分比
        });
        
    } destination:^NSURL * _Nonnull(NSURL * _Nonnull targetPath, NSURLResponse * _Nonnull response) {
        //- block的返回值, 要求返回一个URL, 返回的这个URL就是文件的位置的路径
        NSString *cachesPath = [NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES) lastObject];
        NSString *path = [cachesPath stringByAppendingPathComponent:@"regpic.html"];
        DLog(@"path = %@ filename = %@",path,response.suggestedFilename);
        return [NSURL fileURLWithPath:path];
        
    } completionHandler:^(NSURLResponse * _Nonnull response, NSURL * _Nullable filePath, NSError * _Nullable error) {
        
        UIImage *numberCode = [UIImage imageWithContentsOfFile:[filePath path]];
        [weakSelf.imgButton setTitle:@"" forState:UIControlStateNormal];
        weakSelf.imgButton.layer.borderColor = [UIColor whiteColor].CGColor;
        [weakSelf.imgButton setBackgroundImage:numberCode forState:UIControlStateNormal];
        
    }];
    
    [_downloadTask resume];
}

- (void)textFieldDidChange:(UITextField *)textField {
    //限制手机号输入长度 大陆手机号 +86省去
    if (textField==_phoneTF) {
        if (textField.text.length > 11) {
            textField.text = [textField.text substringToIndex:11];
        }
        if (textField==_imgCodeTF||textField==_reCodeTF) {
            if (textField.text.length > 6) {
                textField.text = [textField.text substringToIndex:6];
            }
        }
        
    }
}
- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    if (textField==_phoneTF||textField==_imgCodeTF||textField==_reCodeTF) {
        textField.keyboardType = UIKeyboardTypeNumberPad;
    }
    
    return YES;
}
-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [self.view endEditing:YES];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
