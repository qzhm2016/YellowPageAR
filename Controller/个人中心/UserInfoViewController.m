//
//  UserInfoViewController.m
//  ARYellowPage
//
//  Created by youdian on 2018/4/16.
//  Copyright © 2018年 YouDianAd. All rights reserved.
//

#import "UserInfoViewController.h"
#import "DetailViewController.h"


@interface UserInfoViewController ()<UITableViewDelegate,UITableViewDataSource>
{
    BOOL isFlag;
    UIImageView *flagView;
}
@property (strong, nonatomic)NSMutableArray *collectList;
@end

@implementation UserInfoViewController


-(void)viewWillAppear:(BOOL)animated{
    //self.navigationController.navigationBar.hidden = YES;
}
- (void)viewDidLoad {
    [super viewDidLoad];
     self.view.backgroundColor = TABLE_COLOR;
    self.navigationItem.title = @"用户";
    [self userCenterUI];
    [self requestCollect];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(requestCollect) name:@"refresh" object:nil];

    
    // Do any additional setup after loading the view.
}
- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}
-(void)requestCollect{
    WS(weakSelf)
    _collectList = [NSMutableArray arrayWithCapacity:0];
    HTTPManager *manager = [HTTPManager httpManager];
    NSString *token = [USER_DEFAULT objectForKey:@"token"];
    [manager getArticleCollectPath:collectPath token:token completion:^(id response) {
        NSArray *data = response[@"data"];
        [data enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            Article *article = [[Article alloc]init];
            [article setValuesForKeysWithDictionary:obj];
            [weakSelf.collectList addObject:article];
        }];
        [weakSelf.tableView reloadData];
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        DLog(@"errorCode = %ld",(long)error.code);
        if (error.code==-1009) {
            WTOAST(@"网络连接错误");
        }else if (error.code==-1011){
            [weakSelf refeshToken];
        }else{
            [AppDel goLoginViewController];
        }
    }];
    
    
}

-(void)refeshToken{
    WS(weakSelf)
    HTTPManager *manager = [HTTPManager httpManager];
    NSString *token = [USER_DEFAULT objectForKey:@"token"];
    [manager refreshUserTokenWithPath:refreshPath old_token:token completion:^(id response) {
        if ([response[@"code"] integerValue ]==0) {
            [USER_DEFAULT setObject:response[@"data"] forKey:@"token"];
            [USER_DEFAULT synchronize];
            [weakSelf requestCollect];
        }
      
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        DLog(@"errorCode = %ld",error.code);
    }];
    
    
}
-(void)userCenterUI{
    UIImageView *imgView=  [[UIImageView alloc]initWithImage:IMAGE_NAME(@"forget_pass_top_iv")];
    _tableView = [[UITableView alloc]initWithFrame:self.view.bounds style:UITableViewStylePlain];
    _tableView.delegate = self;
    _tableView.dataSource = self;
    _tableView.backgroundColor =TABLE_COLOR;
    _tableView.tableHeaderView = imgView;
    [self.view addSubview:_tableView];
    _tableView.tableFooterView = [[UIView alloc]init];
    [_tableView registerClass:[CollectArticleCell class] forCellReuseIdentifier:@"collect"];
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if (isFlag) {
         return _collectList.count;
    }else{
        return 0;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    CollectArticleCell *cell = [tableView dequeueReusableCellWithIdentifier:@"collect" forIndexPath:indexPath];
    Article *a = _collectList[indexPath.row];
    cell.backgroundColor = TABLE_COLOR;
    cell.titleLabel.text = a.title;
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    Article *article = self.collectList[indexPath.row];
    DetailViewController *detail = [DetailViewController new];
    detail.artid = article.id;
    detail.artTitle = article.title;
    detail.status = @"1";
    [self.navigationController pushViewController:detail animated:YES];
    
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 50.0f;
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 55.0f;
}
- (nullable UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    UIView *header = [[UIView alloc]init];
    header.backgroundColor = [UIColor whiteColor];
    UITapGestureRecognizer *tap  = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(tap)];
    [header addGestureRecognizer:tap];
    
    UIImageView *heartImg = [[UIImageView alloc]initWithImage:IMAGE_NAME(@"center_fragment_collect_iv")];
    [header addSubview:heartImg];
    [heartImg mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(header);
        make.size.mas_equalTo(CGSizeMake(28, 28));
        make.left.equalTo(header).offset(25);
    }];
    
    UILabel *recordLabel = [UILabel new];
    recordLabel.text = @"收藏记录";
    recordLabel.textAlignment  = NSTextAlignmentCenter;
    [header addSubview:recordLabel];
    [recordLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(header);
        make.left.equalTo(heartImg.mas_right).offset(20);
        make.size.mas_equalTo(CGSizeMake(80, 30));
    }];
    flagView = [[UIImageView alloc]init];//WithImage:IMAGE_NAME(@"center_fragment_jian_right")];
    flagView.image =isFlag?IMAGE_NAME(@"center_fragment_jian_down"):IMAGE_NAME(@"center_fragment_jian_right");
    [header addSubview:flagView];
    [flagView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(header).offset(-15);
        make.centerY.equalTo(header);
        make.size.mas_equalTo(CGSizeMake(25, 25));
    }];
    return header;
}


-(void)tap {
    isFlag = !isFlag;
    [_tableView reloadData];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/



@end
