//
//  AppDelegate+UM.h
//  ARYellowPage
//
//  Created by youdian on 2018/4/25.
//  Copyright © 2018年 YouDianAd. All rights reserved.
//

#import "AppDelegate.h"

@interface AppDelegate (UM)

-(void)UMConfingure;
-(void)confitUShareSettings;
-(void)configUSharePlatforms;

-(void)configGaoDePlatforms;
@end
