//
//  AppDelegate.m
//  ARYellowPage
//
//  Created by youdian on 2018/4/16.
//  Copyright © 2018年 YouDianAd. All rights reserved.
//

#import "AppDelegate.h"
#import "WelcomeVC.h"
#import "LoginViewController.h"
#import "MainViewController.h"
#import "NavWebViewController.h"
#import "NavPageViewController.h"
#import "ARScanViewController.h"
#import "ViewController.h"
#import "UserInfoViewController.h"
#import "AppDelegate+UM.h"
#import <UMCommon/UMCommon.h>
#import <UShareUI/UShareUI.h>

/**
 @ param
 @ return
 */

#import <easyar/engine.oc.h>
#import "ViewController.h"


NSString * key = @"Lxjf2nXdMEqGLGkPrKG6g4M2ikynHyrFJ1crIIyv9LzaeMnXA98gLBsmVRqK0TvoZnLgqFMVxsi8TALax0fiXEtk0WtVJ3Vu3R2YTRE678TQCLugWXFiaiaAVQxY60VEL7VUGhbWeYZryJntf6rjOLXS86Crs9qbrA6sBVyf1Q9VU3r1oT7PQUc3ep6MOcR1rCpBNV8p";
NSString * cloud_server_address = @"6296c09052012b394de6c002d7b81508.cn1.crs.easyar.com:8080";
NSString * cloud_key = @"a1d3c25006629cf8e3bbabd0c64003dd";
NSString * cloud_secret = @"kYSRElAeH4ceubOnGnBiMqyrHYUgspVHZqBcibifXD10ldVjwhU1RGRwebMjGsry7ItPyreLGBQfG5ZB6IzSKspVmhOeLGQ3ZkB08G7BPE9gSnZyLQiscnHiEOiBS5I8";


@interface AppDelegate ()<UITabBarControllerDelegate>

@end

@implementation AppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    self.window=[[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    //初始化AR
    if (![easyar_Engine initialize:key]) {
        NSLog(@"Initialization Failed.");
    }
    [self UMConfingure];
    [self configUSharePlatforms];
    [self configGaoDePlatforms];
    NSString *value = [USER_DEFAULT objectForKey:@"welcome"];
    if ([value isEqualToString:@"welcome"]) {
        [self setRootWindowControllerTabBar];
    }else{
          [self showWelcomeScrollView];
    }
  
    [self.window makeKeyAndVisible];
    // Override point for customization after application launch.
    return YES;
}


-(void)showWelcomeScrollView{
    self.window.rootViewController = [WelcomeVC new];
}
-(void)goLoginViewController{
    UITabBarController *tabBarController = (UITabBarController *)self.window.rootViewController;
    UINavigationController *loginNav = [[UINavigationController alloc]initWithRootViewController:[LoginViewController new]];
    [tabBarController presentViewController:loginNav animated:YES completion:nil];
}
-(void)setRootWindowControllerTabBar{
    UITabBarController *tabBar = [[UITabBarController alloc]init];
    MainViewController *main  = [MainViewController new];
    UINavigationController *mainNav =[[UINavigationController alloc]initWithRootViewController:main];
    NavWebViewController *web = [NavWebViewController new];
    UINavigationController *webNav = [[UINavigationController alloc]initWithRootViewController:web];
    
    
    NavPageViewController *page =[NavPageViewController new];
    UINavigationController *pageNav = [[UINavigationController alloc]initWithRootViewController:page];
    ARScanViewController *ARScan = [ARScanViewController new];
    UINavigationController *scanNav = [[UINavigationController alloc]initWithRootViewController:ARScan];
    UserInfoViewController *userInfo = [UserInfoViewController new];
    UINavigationController *userNav  = [[UINavigationController alloc]initWithRootViewController:userInfo];
    mainNav.tabBarItem.title = @"首页";
    webNav.title = @"黄页";
   // scanNav.tabBarItem.title = @"AR扫描";
    pageNav.tabBarItem.title = @"推荐";
    userNav.tabBarItem.title = @"个人中心";
    mainNav.tabBarItem.image = IMAGE_NAME(@"main_activity_main_no");
    mainNav.tabBarItem.selectedImage = [IMAGE_NAME(@"main_activity_main_yes") imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
   webNav.tabBarItem.image = IMAGE_NAME(@"main_activity_shop_no");
    webNav.tabBarItem.selectedImage = [IMAGE_NAME(@"main_activity_shop_yes") imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    scanNav.tabBarItem.image = [IMAGE_NAME(@"ficon5") imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    scanNav.tabBarItem.selectedImage = [IMAGE_NAME(@"ficon5") imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    pageNav.tabBarItem.image = IMAGE_NAME(@"ficon3");
   pageNav.tabBarItem.selectedImage = [IMAGE_NAME(@"ficon3h") imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    userNav.tabBarItem.image = IMAGE_NAME(@"main_activity_center_no");
    userNav.tabBarItem.selectedImage = [IMAGE_NAME(@"main_activity_center_yes") imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    tabBar.viewControllers = @[mainNav,webNav,scanNav,pageNav,userNav];
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
    [[UINavigationBar appearance] setTintColor:TABLE_COLOR];
   [[UINavigationBar appearance] setBarTintColor:THEME_COLOR];
    [[UINavigationBar appearance]  setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:[UIColor whiteColor],NSForegroundColorAttributeName,nil]];
//    [[UINavigationBar appearance] setTitleTextAttributes: [NSDictionary dictionaryWithObjectsAndKeys:[UIColor whiteColor], NSForegroundColorAttributeName,[UIFont fontWithName:@"HelveticaNeue-CondensedBlack" size:20.0f], NSFontAttributeName, nil] ];
    tabBar.delegate = self;
    self.window.rootViewController = tabBar;
}
- (void)tabBarController:(UITabBarController *)tabBarController didSelectViewController:(UIViewController *)viewController{
    NSString *token = [USER_DEFAULT objectForKey:@"token"];
    if (tabBarController.selectedIndex==4&&token.length<1) {
        [tabBarController setSelectedIndex:AppDel.selectIndex];
        [self alertLogin];
    }
    
    
    if (tabBarController.selectedIndex==0||tabBarController.selectedIndex==3) {
        AppDel.selectIndex = tabBarController.selectedIndex;
    }
   
    
    
    
    
    
}

-(void)alertLogin{
    
    UIAlertController * alertController = [UIAlertController alertControllerWithTitle:@"温馨提示" message:@"您尚未登录,是否现在登录?" preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction * cancelAction = [UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:nil];
    
    UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDestructive handler:^(UIAlertAction * _Nonnull action){
        
        [self goLoginViewController];
    }];
    [alertController addAction:cancelAction];
    [alertController addAction:okAction];
    [self.window.rootViewController presentViewController:alertController animated:YES completion:nil];
}

// 支持所有iOS系统
- (BOOL)application:(UIApplication *)application openURL:(NSURL *)url sourceApplication:(NSString *)sourceApplication annotation:(id)annotation
{
    //6.3的新的API调用，是为了兼容国外平台(例如:新版facebookSDK,VK等)的调用[如果用6.2的api调用会没有回调],对国内平台没有影响
    BOOL result = [[UMSocialManager defaultManager] handleOpenURL:url sourceApplication:sourceApplication annotation:annotation];
    if (!result) {
        DLog(@"application openURL");
        // 其他如支付等SDK的回调
    }
    return result;
}

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
}


- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}


- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
}


- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}


- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}


@end
