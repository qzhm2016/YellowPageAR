//
//  Created by youdian on 2018/4/25.
//  Copyright © 2018年 YouDianAd. All rights reserved.
//

#import "AppDelegate+UM.h"
#import <UMCommon/UMCommon.h>
#import <UShareUI/UShareUI.h>
#import <AMapFoundationKit/AMapFoundationKit.h>

@implementation AppDelegate (UM)



-(void)UMConfingure{
    [UMConfigure initWithAppkey:@"5ae01e8bf43e485fcd000087" channel:@"App Store"];
}

- (void)confitUShareSettings
{
    /*
     * 打开图片水印
     */
    //[UMSocialGlobal shareInstance].isUsingWaterMark = YES;
    /*
     * 关闭强制验证https，可允许http图片分享，但需要在info.plist设置安全域名
     <key>NSAppTransportSecurity</key>
     <dict>
     <key>NSAllowsArbitraryLoads</key>
     <true/>
     </dict>
     */
    //[UMSocialGlobal shareInstance].isUsingHttpsWhenShareContent = NO;
}

- (void)configUSharePlatforms {
    /* 设置微信的appKey和appSecret */
    [[UMSocialManager defaultManager] setPlaform:UMSocialPlatformType_WechatSession appKey:@"wx6110bdb5d0f84693" appSecret:@"d224eb5e30c39b0296134fdb15284f6d" redirectURL:@"http://mobile.umeng.com/social"];
    /*
     * 移除相应平台的分享，如微信收藏
     */
    //[[UMSocialManager defaultManager] removePlatformProviderWithPlatformTypes:@[@(UMSocialPlatformType_WechatFavorite)]];
    [[UMSocialManager defaultManager] setPlaform:UMSocialPlatformType_QQ appKey:@"1106865186" appSecret:nil redirectURL:nil];
    
    [UMSocialUIManager setPreDefinePlatforms:@[@(UMSocialPlatformType_QQ),@(UMSocialPlatformType_Qzone),@(UMSocialPlatformType_WechatSession),@(UMSocialPlatformType_WechatTimeLine),@(UMSocialPlatformType_WechatFavorite)]];
    
    
}

// 支持所有iOS系统
- (BOOL)application:(UIApplication *)application openURL:(NSURL *)url sourceApplication:(NSString *)sourceApplication annotation:(id)annotation
{
    //6.3的新的API调用，是为了兼容国外平台(例如:新版facebookSDK,VK等)的调用[如果用6.2的api调用会没有回调],对国内平台没有影响
    BOOL result = [[UMSocialManager defaultManager] handleOpenURL:url sourceApplication:sourceApplication annotation:annotation];
    if (!result) {
        DLog(@"application openURL");
        // 其他如支付等SDK的回调
    }
    return result;
}



/**
 高德地图
 @ param
 @ return
 */
-(void)configGaoDePlatforms{
     [AMapServices sharedServices].apiKey =@"2bfe0eeb421257e3104506bca0a21019";
}
@end
