//
//  AppDelegate.h
//  ARYellowPage
//
//  Created by youdian on 2018/4/16.
//  Copyright © 2018年 YouDianAd. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (copy, nonatomic)NSString *cityid;
@property (assign,nonatomic)NSInteger selectIndex;


-(void)goLoginViewController;
-(void)setRootWindowControllerTabBar;
-(void)alertLogin;
@end

