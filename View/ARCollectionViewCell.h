//
//  ARCollectionViewCell.h
//  ARYellowPage
//
//  Created by youdian on 2018/4/19.
//  Copyright © 2018年 YouDianAd. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ARCollectionViewCell : UICollectionViewCell

@end

@interface NavCollectionViewCell : UICollectionViewCell

@property (strong, nonatomic)UIImageView *logoView;
@property (strong, nonatomic)UILabel *titleLabel;
@end


@interface CateCollectionCell : UICollectionViewCell

@property (strong, nonatomic)UIImageView *logoView;
@end




