//
//  ARTableViewCell.m
//  ARYellowPage
//
//  Created by youdian on 2018/4/19.
//  Copyright © 2018年 YouDianAd. All rights reserved.
//

#import "ARTableViewCell.h"

@implementation ARTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}




@end
//分类
@implementation CateTableViewCell
- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}


-(id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        WS(weakSelf)
        _logoView = [[UIImageView alloc]init];
        [self addSubview:_logoView];
        [_logoView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerY.equalTo(weakSelf);
            make.left.equalTo(weakSelf).offset(15);
            make.size.mas_equalTo(CGSizeMake(95, 95));
        }];
        _nameLabel = [UILabel new];
        _nameLabel.font =[UIFont systemFontOfSize:17.0f];
        _nameLabel.userInteractionEnabled=YES;
        [self addSubview:_nameLabel];
        [_nameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(weakSelf.logoView.mas_right).offset(10);
            make.top.equalTo(weakSelf).offset(10);
            make.size.mas_equalTo(CGSizeMake(220, 30));
        }];
        _telLabel  = [UILabel new];
        _telLabel.userInteractionEnabled  = YES;
        _telLabel.textColor = [UIColor grayColor];
        _telLabel.font = [UIFont systemFontOfSize:15.0f];
        [self addSubview:_telLabel];
        [_telLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(weakSelf.logoView.mas_right).offset(10);
            make.top.equalTo(weakSelf.nameLabel.mas_bottom).offset(0);
            make.size.mas_equalTo(CGSizeMake(175, 25));
        }];
        _addressLbel  = [UILabel new];
        _addressLbel.font = [UIFont systemFontOfSize:15.0f];
        _addressLbel.userInteractionEnabled = YES;
       _addressLbel.textColor = [UIColor grayColor];
        _addressLbel.numberOfLines = 0;
        [self addSubview:_addressLbel ];
        [_addressLbel  mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(weakSelf.logoView.mas_right).offset(10);
            make.top.equalTo(weakSelf.telLabel.mas_bottom);
            make.size.mas_equalTo(CGSizeMake(SCREEN_WIDTH-130, 45));
        }];
    }
    
 return self;

}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}


@end


@implementation CollectArticleCell

-(id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        WS(weakSelf)
        _titleLabel = [UILabel new];
        _titleLabel.textColor = [UIColor lightGrayColor];
        [self addSubview:_titleLabel];
        [_titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(weakSelf).offset(25);
            make.centerY.equalTo(weakSelf);
            make.size.mas_equalTo(CGSizeMake(SCREEN_WIDTH-80, 30));
        }];
        _arrowImgView = [[UIImageView alloc]initWithImage:IMAGE_NAME(@"center_fragment_jian_right")];
        [self addSubview:_arrowImgView];
        [_arrowImgView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerY.equalTo(weakSelf);
            make.right.equalTo(weakSelf).offset(-15);
            make.size.mas_equalTo(CGSizeMake(25, 25));
        }];
        
    }
    
    return self;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}

@end

@implementation LunListTableViewCell

-(id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        WS(weakSelf)
        _logoView = [[UIImageView alloc]init];
       _logoView.layer.cornerRadius = 8.0f;
        _logoView.clipsToBounds = YES;
        [self addSubview:_logoView];
        [_logoView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.center.equalTo(weakSelf);
            make.size.mas_equalTo(CGSizeMake(SCREEN_WIDTH-20, 160));
           
        }];
        
        _titleLabel = [UILabel new];
        _titleLabel.textAlignment = NSTextAlignmentCenter;
        _titleLabel.backgroundColor = [UIColor blackColor];
        _titleLabel.textColor = [UIColor whiteColor];
        [self.logoView addSubview:_titleLabel];
        [_titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(weakSelf.logoView).offset(-0);
            make.right.equalTo(weakSelf.logoView).offset(0);
            make.bottom.equalTo(weakSelf.logoView).offset(3);
            make.height.mas_equalTo(45);
        }];
        
        
    }
    self.clipsToBounds = YES;
    return self;
}
- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}
@end

