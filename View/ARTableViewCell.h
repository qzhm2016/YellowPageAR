//
//  ARTableViewCell.h
//  ARYellowPage
//
//  Created by youdian on 2018/4/19.
//  Copyright © 2018年 YouDianAd. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ARTableViewCell : UITableViewCell

@end
//分类
@interface CateTableViewCell : UITableViewCell

@property (strong, nonatomic)UIImageView *logoView;
@property (strong, nonatomic)UILabel *nameLabel;
@property (nonatomic ,strong) UILabel*telLabel;
@property (nonatomic ,strong) UILabel *addressLbel;
@end

@interface CollectArticleCell : UITableViewCell

@property (strong, nonatomic)UILabel *titleLabel;
@property (strong, nonatomic)UIImageView *arrowImgView;
@end

//lunList
@interface LunListTableViewCell : UITableViewCell

@property (strong, nonatomic)UIImageView *logoView;
@property (strong, nonatomic)UILabel *titleLabel;
@end
