//
//  ARCollectionViewCell.m
//  ARYellowPage
//
//  Created by youdian on 2018/4/19.
//  Copyright © 2018年 YouDianAd. All rights reserved.
//

#import "ARCollectionViewCell.h"

@implementation ARCollectionViewCell


@end



@implementation NavCollectionViewCell

-(id)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        WS(weakSelf)
        _logoView = [[UIImageView alloc]init];
        _logoView.layer.cornerRadius = 12.0f;
       
        [self addSubview:_logoView];
        [_logoView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(weakSelf);
            make.left.right.bottom.equalTo(weakSelf);
            
        }];
        _titleLabel = [UILabel new];
        _titleLabel.backgroundColor =RGBA(0, 0, 0, 0.9f);
        _titleLabel.clipsToBounds = YES;
        _titleLabel.textColor = [UIColor whiteColor];
        _titleLabel.textAlignment = NSTextAlignmentCenter;
        _titleLabel.font = [UIFont systemFontOfSize:18.0f];
        [self.logoView addSubview:_titleLabel];
         _logoView.clipsToBounds = YES;
        [_titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(weakSelf.logoView).offset(-5);
            make.right.equalTo(weakSelf.logoView).offset(5);
            make.bottom.equalTo(weakSelf.logoView).offset(5);
            make.height.mas_equalTo(38);
        }];
    }
    self.clipsToBounds = YES;
    return self;
}

@end

@implementation CateCollectionCell
-(id)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        WS(weakSelf)
        _logoView = [[UIImageView alloc]init];
        _logoView.layer.cornerRadius = 8.0f;
        _logoView.clipsToBounds = YES;
        [self addSubview:_logoView];
        [_logoView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.center.equalTo(weakSelf);
            make.size.equalTo(weakSelf);
        }];
}
    return self;
}

@end





